package com.dtu.transaction.application;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.transaction.TransactionRejectedEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.domain.BankService;
import com.dtu.domain.BankServiceException;
import com.dtu.domain.BankServiceException_Exception;
import com.dtu.transaction.domain.Transaction;
import com.dtu.transaction.domain.repositories.TransactionRepository;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Consumer;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public class TransactionServiceTests {
  private final TransactionRepository repository = mock(TransactionRepository.class);
  private final BankService bank = mock(BankService.class);
  private final EventDispatcher dispatcher = mock(EventDispatcher.class);
  private final EventPublisher publisher = mock(EventPublisher.class);

  private final TransactionService service =
      new TransactionService(repository, bank, dispatcher, publisher);

  public static <A> void assertCaptureSatisfies(Consumer<ArgumentCaptor<A>> captors,
      Consumer<A> a,
      Class<A> aa) {
    final ArgumentCaptor<A> captor = ArgumentCaptor.forClass(aa);
    captors.accept(captor);
    captor.getAllValues().forEach($ -> assertThat($).satisfies(a));
  }

  @Test
  void invalidTransactionInitialized() {
    //GIVEN
    final String testFlow = "testFlowId";
    final String token = "test_token";
    final String merchantId = "test_merchant_id";
    final BigDecimal invalidValue = BigDecimal.valueOf(-1000);

    final TransactionRequestedEvent event =
        new TransactionRequestedEvent(token, merchantId, invalidValue);
    event.setFlowId(testFlow);

    //WHEN
    service.apply(event);

    //THEN
    assertCaptureSatisfies(
        $ -> verify(publisher).publish($.capture()),
        e -> assertEquals(testFlow, e.getFlowId()),
        TransactionRejectedEvent.class);
  }

  @Test
  void validTransactionInitialized() {
    //GIVEN
    final String testFlow = "testFlowId";
    final String token = "test_token";
    final String merchantId = "test_merchant_id";
    final BigDecimal invalidValue = BigDecimal.valueOf(-1000);

    final TransactionRequestedEvent event =
        new TransactionRequestedEvent(token, merchantId, invalidValue);
    event.setFlowId(testFlow);

    //WHEN
    service.apply(event);

    //THEN
    verify(publisher, never()).publish(event);
  }

  @Test
  void transactionNotFound_shouldTransferRejectedEventBePublished() {
    //GIVEN
    final Consumer<Transaction> consumer = $ -> $.setCustomerAccount("test_customer_account");

    //WHEN
    when(repository.getById(any())).thenReturn(Optional.empty());
    service.attemptPayment("test_process_id", consumer);

    //THEN
    assertCaptureSatisfies(
        $ -> verify(publisher).publish($.capture()),
        e -> assertEquals("Unexpected error - transaction was corrupted.",
            e.getMessage()),
        TransactionRejectedEvent.class);
  }

  @Test
  void transactionUnready_shouldTransactionNotBeSaved() {
    //GIVEN
    final Consumer<Transaction> consumer = $ -> $.setCustomerAccount("test_customer_account");
    final Transaction transaction = mock(Transaction.class);

    //WHEN
    when(repository.getById(any())).thenReturn(Optional.of(transaction));
    service.attemptPayment("test_process_id", consumer);
    when(transaction.isReadyToPay()).thenReturn(false);

    //THEN
    verify(repository, never()).save(transaction);
  }

  @Test
  void transactionReady_shouldTransactionBeSaved() {
    //GIVEN
    final Consumer<Transaction> consumer = $ -> $.setCustomerAccount("test_customer_account");
    final Transaction transaction = mock(Transaction.class);

    //WHEN
    when(repository.getById("test_process_id")).thenReturn(Optional.of(transaction));
    when(transaction.isReadyToPay()).thenReturn(true);
    service.attemptPayment("test_process_id", consumer);

    //THEN
    verify(repository).save(transaction);
  }

  @Test
  void bankTransferFail_shouldPaymentBeFailed() throws BankServiceException_Exception {
    //GIVEN
    final Transaction transaction = mock(Transaction.class);
    when(transaction.getCustomerAccount()).thenReturn("test_customer_account");
    when(transaction.getMerchantAccount()).thenReturn("test_merchant_account");
    when(transaction.getAmount()).thenReturn(BigDecimal.valueOf(1000));

    //WHEN
    doThrow(
        new BankServiceException_Exception("Cannot execute transfer", new BankServiceException()))
        .when(bank)
        .transferMoneyFromTo(anyString(), anyString(), any(BigDecimal.class), anyString());
    service.pay(transaction);

    //THEN
    verify(transaction).paymentFailed("Bank rejected transfer");
    verify(transaction, never()).paymentSuccess();
  }

  @Test
  void bankTransferSuccess_shouldPaymentBeSucceed() throws BankServiceException_Exception {
    //GIVEN
    final Transaction transaction = mock(Transaction.class);
    when(transaction.getCustomerAccount()).thenReturn("test_customer_account");
    when(transaction.getMerchantAccount()).thenReturn("test_merchant_account");
    when(transaction.getAmount()).thenReturn(BigDecimal.valueOf(1000));

    //WHEN
    doNothing().when(bank)
        .transferMoneyFromTo(anyString(), anyString(), any(BigDecimal.class), anyString());
    service.pay(transaction);

    //THEN
    verify(transaction).paymentSuccess();
    verify(transaction, never()).paymentFailed("Test fail readon");
  }
}
