package com.dtu.transaction.domain;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerBankAccountIdRetrievedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdRetrievedEvent;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public class TransactionTests {
  private final String validFlow = "valid_transaction_flow";
  private final String invalidFlow = "invalid_transaction_flow";

  private final List<BusinessEvent> validTransaction = Arrays.asList(
    new TransactionRequestedEvent("test_token", "test_merchant_id", BigDecimal.valueOf(1000)).setFlowId(validFlow),
    new CustomerBankAccountIdRetrievedEvent("test_customer_account").setFlowId(validFlow),
    new MerchantBankAccountIdRetrievedEvent("test_merchant_account").setFlowId(validFlow)
  );

  private final List<BusinessEvent> invalidTransaction = Arrays.asList(
      new TransactionRequestedEvent("test_token", "test_merchant_id", BigDecimal.valueOf(-1000)).setFlowId(invalidFlow),
      new CustomerBankAccountIdRetrievedEvent("").setFlowId(invalidFlow),
      new MerchantBankAccountIdRetrievedEvent("").setFlowId(invalidFlow)
  );

  @Test
  void validTransaction(){
    //GIVEN
    final Optional<Transaction> optional = Transaction.createFromEvents(validTransaction);
    assertFalse(optional.isEmpty());
    final Transaction transaction = optional.get();

    //THEN
    assertTrue(transaction.validateCustomerAccount());
    assertTrue(transaction.validateMerchantAccount());
    assertTrue(transaction.validateMoney());
  }

  @Test
  void invalidTransaction(){
    //GIVEN
    final Optional<Transaction> optional = Transaction.createFromEvents(invalidTransaction);
    assertFalse(optional.isEmpty());
    final Transaction transaction = optional.get();

    //THEN
    assertFalse(transaction.validateCustomerAccount());
    assertFalse(transaction.validateMerchantAccount());
    assertFalse(transaction.validateMoney());
  }
}
