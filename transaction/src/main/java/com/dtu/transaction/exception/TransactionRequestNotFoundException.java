package com.dtu.transaction.exception;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public class TransactionRequestNotFoundException extends RuntimeException {
  public TransactionRequestNotFoundException(String message) {
    super(message);
  }
}
