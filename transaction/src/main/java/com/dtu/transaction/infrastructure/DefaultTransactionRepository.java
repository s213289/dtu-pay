package com.dtu.transaction.infrastructure;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.transaction.domain.Transaction;
import com.dtu.transaction.domain.repositories.EventStore;
import com.dtu.transaction.domain.repositories.TransactionRepository;
import java.util.List;
import java.util.Optional;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public class DefaultTransactionRepository implements TransactionRepository {
  private final EventStore eventStore;

  public DefaultTransactionRepository(EventStore eventStore) {
    this.eventStore = eventStore;
  }

  @Override public void save(Transaction transaction) {
    eventStore.addEvents(transaction.getProcessId(), transaction.getAppliedEvents());
    transaction.clearAppliedEvents();
  }

  @Override public Optional<Transaction> getById(String flowId) {
    List<BusinessEvent> events = eventStore.getEventsFor(flowId);
    if (!events.isEmpty()) {
      return Transaction.createFromEvents(events);
    } else {
      return Optional.empty();
    }
  }
}
