package com.dtu.transaction.application;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.domain.BankService;
import com.dtu.domain.BankServiceService;
import com.dtu.rabbitmq.receiver.RabbitMqEventDispatcher;
import com.dtu.rabbitmq.sender.RabbitMqEventPublisher;
import com.dtu.transaction.domain.repositories.EventStore;
import com.dtu.transaction.domain.repositories.TransactionRepository;
import com.dtu.transaction.infrastructure.DefaultTransactionRepository;
import io.quarkus.runtime.Startup;
import javax.enterprise.context.ApplicationScoped;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public class Application {
  @Startup @ApplicationScoped
  public TransactionService transactionService(TransactionRepository repository,
      BankService bankService, EventDispatcher dispatcher, EventPublisher publisher) {
    return new TransactionService(repository, bankService, dispatcher, publisher);
  }

  @ApplicationScoped
  public BankService bankService() {
    return new BankServiceService().getBankServicePort();
  }

  @Startup @ApplicationScoped
  public EventStore eventStore(EventDispatcher dispatcher, EventPublisher publisher) {
    return new EventStore(dispatcher, publisher);
  }

  @ApplicationScoped
  public TransactionRepository transactionRepository(EventStore eventStore) {
    return new DefaultTransactionRepository(eventStore);
  }

  @ApplicationScoped
  public EventPublisher eventPublisher() {
    return new RabbitMqEventPublisher();
  }

  @ApplicationScoped
  public EventDispatcher eventDispatcher() {
    return new RabbitMqEventDispatcher();
  }
}
