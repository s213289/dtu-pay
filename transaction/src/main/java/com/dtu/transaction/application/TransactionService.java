package com.dtu.transaction.application;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.token.TokenLookupFailedEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRejectedEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerBankAccountIdRetrievedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdLookupFailedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdRetrievedEvent;
import com.dtu.domain.BankService;
import com.dtu.domain.BankServiceException_Exception;
import com.dtu.transaction.domain.Transaction;
import com.dtu.transaction.domain.repositories.TransactionRepository;
import com.dtu.transaction.exception.TransactionRequestNotFoundException;
import com.google.common.annotations.VisibleForTesting;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import lombok.extern.slf4j.Slf4j;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import org.jmolecules.ddd.annotation.Service;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@Service
@Slf4j
public class TransactionService {
  // --- INTERNAL
  private final TransactionRepository repository;
  private final EventDispatcher dispatcher;
  private final EventPublisher publisher;

  // --- EXTERNAL
  private final BankService bankService;

  public TransactionService(TransactionRepository repository, BankService bankService,
      EventDispatcher dispatcher, EventPublisher publisher) {
    this.repository = repository;
    this.bankService = bankService;
    this.dispatcher = dispatcher;
    this.publisher = publisher;

    dispatcher.register(TransactionRequestedEvent.class,
        $ -> apply((TransactionRequestedEvent) $));
    dispatcher.register(CustomerBankAccountIdRetrievedEvent.class,
        $ -> apply((CustomerBankAccountIdRetrievedEvent) $));
    dispatcher.register(MerchantBankAccountIdRetrievedEvent.class,
        $ -> apply((MerchantBankAccountIdRetrievedEvent) $));
    dispatcher.register(MerchantBankAccountIdLookupFailedEvent.class,
        $ -> apply((MerchantBankAccountIdLookupFailedEvent) $));
    dispatcher.register(TokenLookupFailedEvent.class,
        $ -> apply((TokenLookupFailedEvent) $));
  }

  // --- Failsafe Policy
  private final RetryPolicy<Object> retryPolicy = new RetryPolicy<>()
      .handle(TransactionRequestNotFoundException.class)
      .withDelay(Duration.of(1, ChronoUnit.SECONDS))
      .withMaxRetries(5);

  // --- Business Logic
  @VisibleForTesting void apply(TransactionRequestedEvent event) {
    Optional<Transaction> transaction = Transaction.createFromEvents(List.of(event));
    if (transaction.isEmpty() || !transaction.get().validateMoney()) {
      publisher.publish(new TransactionRejectedEvent(
          "Invalid transaction request, please check amount input.").setFlowId(event.getFlowId()));
    }
  }

  private void apply(MerchantBankAccountIdLookupFailedEvent event) {
    publisher.publish(new TransactionRejectedEvent("Merchant account is not valid.").setFlowId(
        event.getFlowId()));
  }

  private void apply(TokenLookupFailedEvent event) {
    publisher.publish(
        new TransactionRejectedEvent("Token is invalid.").setFlowId(event.getFlowId()));
  }

  private void apply(CustomerBankAccountIdRetrievedEvent event) {
    attemptPayment(event.getFlowId(), $ -> $.apply(event));
  }

  private void apply(MerchantBankAccountIdRetrievedEvent event) {
    attemptPayment(event.getFlowId(), $ -> $.apply(event));
  }

  @VisibleForTesting void attemptPayment(String processId, Consumer<Transaction> consumer) {
    try {
      Failsafe
          .with(retryPolicy)
          .run(() -> {
                Optional<Transaction> optional = repository.getById(processId);
                if (optional.isEmpty()) {
                  throw new TransactionRequestNotFoundException(
                      "Transaction request with ID: " + processId + " was not found");
                }
                Transaction transaction = optional.get();
                consumer.accept(transaction);
                if (transaction.isReadyToPay()) {
                  pay(transaction);
                  repository.save(transaction);
                }
              }
          );
    } catch (TransactionRequestNotFoundException e) {
      publisher.publish(new TransactionRejectedEvent(
          "Unexpected error - transaction was corrupted.").setFlowId(processId));
    }
  }

  @VisibleForTesting void pay(Transaction transaction) {
    try {
      log.info("Attempt bank transfer for flow: {}. Transaction: {}", transaction.getProcessId(),
          transaction);
      bankService.transferMoneyFromTo(transaction.getCustomerAccount(),
          transaction.getMerchantAccount(), transaction.getAmount(), "Money Transfer");
      transaction.paymentSuccess();
    } catch (BankServiceException_Exception e) {
      log.error("Transaction from: {} to: {}, amount: {} could not be finalized.",
          transaction.getMerchantAccount(), transaction.getCustomerAccount(),
          transaction.getAmount());
      transaction.paymentFailed("Bank rejected transfer");
    }
  }
}
