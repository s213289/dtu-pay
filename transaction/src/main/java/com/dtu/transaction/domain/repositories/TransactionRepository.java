package com.dtu.transaction.domain.repositories;

import com.dtu.transaction.domain.Transaction;
import java.util.Optional;
import org.jmolecules.ddd.annotation.Repository;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@Repository
public interface TransactionRepository {
  void save(Transaction transaction);

  Optional<Transaction> getById(String flowId);
}
