package com.dtu.transaction.domain.repositories;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.base.domain.model.event.token.TokenLookupFailedEvent;
import com.dtu.base.domain.model.event.token.TokenLookupSuccessEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerBankAccountIdRetrievedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdLookupFailedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdRetrievedEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@Slf4j
public class EventStore {
  private final Map<String, List<BusinessEvent>> store;
  private final EventDispatcher dispatcher;
  private final EventPublisher publisher;

  public EventStore(EventDispatcher dispatcher, EventPublisher publisher) {
    this.store = new ConcurrentHashMap<>();
    this.dispatcher = dispatcher;
    this.publisher = publisher;
    dispatcher.register(TransactionRequestedEvent.class,
        $ -> persistEvent($.getFlowId(), $));
    dispatcher.register(TokenLookupSuccessEvent.class,
        $ -> persistEvent($.getFlowId(), $));
    dispatcher.register(CustomerBankAccountIdRetrievedEvent.class,
        $ -> persistEvent($.getFlowId(), $));
    dispatcher.register(MerchantBankAccountIdRetrievedEvent.class,
        $ -> persistEvent($.getFlowId(), $));
    dispatcher.register(TokenLookupFailedEvent.class,
        $ -> persistEvent($.getFlowId(), $));
    dispatcher.register(MerchantBankAccountIdLookupFailedEvent.class,
        $ -> persistEvent($.getFlowId(), $));
  }

  private void persistEvent(String processId, BusinessEvent event) {
    if (!store.containsKey(processId)) {
      store.put(processId, new LinkedList<>());
    }
    store.get(processId).add(event);
  }

  public void addEvent(String processId, BusinessEvent event) {
    persistEvent(processId, event);
    publisher.publish(event);
  }

  public void addEvents(String processId, List<BusinessEvent> appliedEvents) {
    appliedEvents.forEach($ -> addEvent(processId, $));
  }

  public List<BusinessEvent> getEventsFor(String flowId) {
    return store.getOrDefault(flowId, new LinkedList<>());
  }
}
