package com.dtu.transaction.domain;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.base.domain.model.event.token.TokenLookupFailedEvent;
import com.dtu.base.domain.model.event.token.TokenLookupSuccessEvent;
import com.dtu.base.domain.model.event.transaction.TransactionFinishedEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRejectedEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerBankAccountIdRetrievedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdLookupFailedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdRetrievedEvent;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import lombok.Getter;
import lombok.Setter;
import org.jmolecules.ddd.annotation.AggregateRoot;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@AggregateRoot
@Getter
public class Transaction {
  private String processId;
  private BigDecimal amount;
  private String token;
  private String customerId;
  private String merchantId;
  @Setter private String customerAccount;
  @Setter private String merchantAccount;

  private final List<BusinessEvent> appliedEvents;

  private final Map<Class<? extends BusinessEvent>, Consumer<BusinessEvent>> handlers =
      new HashMap<>();

  public Transaction() {
    this.appliedEvents = new LinkedList<>();
    registerEventHandlers();
  }

  private void registerEventHandlers() {
    handlers.put(TransactionRequestedEvent.class, $ -> apply((TransactionRequestedEvent) $));
    handlers.put(TokenLookupSuccessEvent.class,
        $ -> apply((TokenLookupSuccessEvent) $));
    handlers.put(CustomerBankAccountIdRetrievedEvent.class,
        $ -> apply((CustomerBankAccountIdRetrievedEvent) $));
    handlers.put(MerchantBankAccountIdRetrievedEvent.class,
        $ -> apply((MerchantBankAccountIdRetrievedEvent) $));
    handlers.put(TokenLookupFailedEvent.class, $ -> apply((TokenLookupFailedEvent) $));
    handlers.put(MerchantBankAccountIdLookupFailedEvent.class,
        $ -> apply((MerchantBankAccountIdLookupFailedEvent) $));
  }

  public static Optional<Transaction> createFromEvents(List<BusinessEvent> events) {
    try {
      Transaction transaction = new Transaction();
      transaction.applyEvents(events);

      assert transaction.getProcessId() != null;

      return Optional.of(transaction);
    } catch (IllegalStateException | AssertionError e) {
      return Optional.empty();
    }
  }

  // --- Business Logic

  public void paymentSuccess() {
    appliedEvents.add(
        new TransactionFinishedEvent(token, customerId, merchantId, customerAccount, merchantAccount, amount).setFlowId(processId));
  }

  public void paymentFailed(String reason) {
    appliedEvents.add(new TransactionRejectedEvent(reason).setFlowId(processId));
  }

  public boolean isReadyToPay() {
    return validateMoney() && validateCustomerAccount() && validateMerchantAccount();
  }

  public boolean validateMoney() {
    return amount != null && amount.compareTo(BigDecimal.ZERO) > 0;
  }

  public boolean validateCustomerAccount() {
    return validateAccountNumber(customerAccount);
  }

  public boolean validateMerchantAccount() {
    return validateAccountNumber(merchantAccount);
  }

  private boolean validateAccountNumber(String accountNumber) {
    return accountNumber != null && !accountNumber.isEmpty();
  }

  // --- Event Handling

  private void applyEvents(List<BusinessEvent> events) throws Error {
    events.forEach(this::applyEvent);
  }

  private void applyEvent(BusinessEvent e) {
    handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e);
  }

  private void missingHandler(BusinessEvent e) {
    throw new Error("handler for event " + e + " missing");
  }

  public void apply(TransactionRequestedEvent event) {
    this.processId = event.getFlowId();
    this.amount = event.getAmount();
    this.token = event.getToken();
    this.merchantId = event.getMerchantId();
  }

  private void apply(TokenLookupSuccessEvent event) {
    this.customerId = event.getCustomerId().toString();
  }

  public void apply(CustomerBankAccountIdRetrievedEvent event) {
    this.customerAccount = event.getBankAccountId();
  }

  public void apply(TokenLookupFailedEvent event) {
    throw new IllegalStateException(
        "Transaction cannot be retrieved, customer token was not valid.");
  }

  public void apply(MerchantBankAccountIdRetrievedEvent event) {
    this.merchantAccount = event.getBankAccountId();
  }

  public void apply(MerchantBankAccountIdLookupFailedEvent event) {
    throw new IllegalStateException(
        "Transaction cannot be retrieved, merchant account number was not found.");
  }

  public void clearAppliedEvents() {
    appliedEvents.clear();
  }
}
