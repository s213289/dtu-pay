package com.dtu.user.service;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.rabbitmq.receiver.RabbitMqEventDispatcher;
import com.dtu.rabbitmq.sender.RabbitMqEventPublisher;
import com.dtu.user.repositories.InMemoryUserRepository;
import com.dtu.user.repositories.ReadModelUserRepository;
import com.dtu.user.repositories.UserEventStore;
import io.quarkus.runtime.Startup;
import javax.enterprise.context.ApplicationScoped;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
public class Application {
  @Startup @ApplicationScoped
  public UserService userService(InMemoryUserRepository userRepository, ReadModelUserRepository readUserRepository, EventDispatcher eventDispatcher, EventPublisher eventPublisher) {
    return new UserService(userRepository, readUserRepository, eventDispatcher, eventPublisher);
  }

  @ApplicationScoped
  public UserEventStore eventStore() {
    return new UserEventStore();
  }

  @ApplicationScoped
  public InMemoryUserRepository userRepository(UserEventStore eventStore) {
    return new InMemoryUserRepository(eventStore);
  }

  @ApplicationScoped
  public ReadModelUserRepository transactionRepository(UserEventStore eventStore) {
    return new ReadModelUserRepository(eventStore);
  }

  @ApplicationScoped
  public EventPublisher eventPublisher() {
    return new RabbitMqEventPublisher();
  }

  @ApplicationScoped
  public EventDispatcher eventDispatcher() {
    return new RabbitMqEventDispatcher();
  }

}
