package com.dtu.user.service;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.token.TokenLookupSuccessEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerBankAccountIdRetrievedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationResponseEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdLookupFailedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdRetrievedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationRequestedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationResponseEvent;
import com.dtu.user.aggregate.Client;
import com.dtu.user.aggregate.Merchant;
import com.dtu.user.contracts.UserRepository;
import com.dtu.user.repositories.ReadModelUserRepository;
import com.google.common.annotations.VisibleForTesting;
import lombok.extern.slf4j.Slf4j;
import org.jmolecules.ddd.annotation.Service;

import java.util.UUID;

@Service
@Slf4j
public class UserService {
    private final UserRepository userRepository;
    private final ReadModelUserRepository readModelUserRepository;
    private final EventDispatcher eventDispatcher;
    private final EventPublisher eventPublisher;

    public UserService(UserRepository userRepository, ReadModelUserRepository readModelUserRepository, EventDispatcher eventDispatcher, EventPublisher eventPublisher) {
        this.userRepository = userRepository;
        this.readModelUserRepository = readModelUserRepository;
        this.eventDispatcher = eventDispatcher;
        this.eventPublisher = eventPublisher;

        this.eventDispatcher.register(CustomerRegistrationRequestedEvent.class,
                $ -> apply((CustomerRegistrationRequestedEvent) $));
        this.eventDispatcher.register(MerchantRegistrationRequestedEvent.class,
                $ -> apply((MerchantRegistrationRequestedEvent) $));
        this.eventDispatcher.register(TransactionRequestedEvent.class,
                $ -> apply((TransactionRequestedEvent) $));
        this.eventDispatcher.register(TokenLookupSuccessEvent.class,
                $ -> apply((TokenLookupSuccessEvent) $));
    }

    /**
     * @author Bartłomiej Józef Otto - s203042
     */
    @VisibleForTesting
    public void apply(CustomerRegistrationRequestedEvent event) {
        Client client = Client.create(event);
        registerClient(client, event.getFlowId());
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @VisibleForTesting
    public void apply(MerchantRegistrationRequestedEvent event) {
        Merchant merchant = Merchant.create(event);
        registerMerchant(merchant, event.getFlowId());
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @VisibleForTesting
    public void apply(TransactionRequestedEvent event) {
        String bankAccountID = getMerchantBankAccountIdByUserId(UUID.fromString(event.getMerchantId()));
        if(bankAccountID != null ){
            this.eventPublisher.publish(new MerchantBankAccountIdRetrievedEvent(bankAccountID)
                    .setFlowId(event.getFlowId()));
        }
        else {
            this.eventPublisher.publish(new MerchantBankAccountIdLookupFailedEvent()
                    .setFlowId((event.getFlowId())));
        }
    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    @VisibleForTesting
    public void apply(TokenLookupSuccessEvent event) {
        String bankAccountID = getCustomerBankAccountIdByUserId(event.getCustomerId());
        this.eventPublisher.publish(new CustomerBankAccountIdRetrievedEvent(bankAccountID)
                .setFlowId(event.getFlowId()));
    }

    /* Commands */

    /**
     * @author Marta Ewa Lech - s212481
     */
    private void registerClient(Client client, String flowId) {
        userRepository.registerUser(client);
        UUID clientId = getClientId(client);
        this.eventPublisher.publish(new CustomerRegistrationResponseEvent(clientId.toString())
                .setFlowId(flowId));
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    private void registerMerchant(Merchant merchant, String flowId) {
        userRepository.registerUser(merchant);
        UUID merchantId = getMerchantId(merchant);
        this.eventPublisher.publish(new MerchantRegistrationResponseEvent(merchantId.toString())
                .setFlowId(flowId));
    }

    /* Queries */
    /**
     * @author Marta Ewa Lech - s212481
     */
    private UUID getClientId(Client client) {
        return readModelUserRepository.getClientId(client.getID());
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    private UUID getMerchantId(Merchant client) {
        return readModelUserRepository.getMerchantId(client.getID());
    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    private String getCustomerBankAccountIdByUserId(UUID userId) {
        return readModelUserRepository.getCustomerBankAccountIdByUserId(userId);
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    private String getMerchantBankAccountIdByUserId(UUID userId) {
        return readModelUserRepository.getMerchantBankAccountIdByUserId(userId);
    }
}
