package com.dtu.user.repositories;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.user.aggregate.Client;
import com.dtu.user.aggregate.Merchant;
import com.dtu.user.contracts.ReadUserRepository;

import java.util.*;

/**
 * @author Marta Ewa Lech - s212481
 */
public class ReadModelUserRepository implements ReadUserRepository {
    private final UserEventStore eventStore;

    public ReadModelUserRepository(UserEventStore eventStore) {
        this.eventStore = eventStore;
    }

    public boolean doesUserExists(String accountId){
        return true;
    }

    public UUID getClientId(UUID userId) {
        List<BusinessEvent> events = eventStore.getEventsFor(userId.toString());
        if (!events.isEmpty()) {
            return Client.createFromEvents(events).orElseThrow().getID();
        } else {
            return null;
        }
    }

    public UUID getMerchantId(UUID userId) {
        List<BusinessEvent> events = eventStore.getEventsFor(userId.toString());
        if (!events.isEmpty()) {
            return Merchant.createFromEvents(events).orElseThrow().getID();
        } else {
            return null;
        }
    }

    public String getCustomerBankAccountIdByUserId(UUID userId) {
        List<BusinessEvent> events = eventStore.getEventsFor(userId.toString());
        if (!events.isEmpty()) {
            return Client.createFromEvents(events).orElseThrow().getBankAccountID();
        } else {
            return null;
        }
    }

    public String getMerchantBankAccountIdByUserId(UUID userId) {
        List<BusinessEvent> events = eventStore.getEventsFor(userId.toString());
        if (!events.isEmpty()) {
            return Merchant.createFromEvents(events).get().getBankAccountID();
        } else {
            return null;
        }
    }
}
