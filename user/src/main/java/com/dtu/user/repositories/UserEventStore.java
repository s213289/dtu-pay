package com.dtu.user.repositories;

import com.dtu.base.domain.model.BusinessEvent;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
public class UserEventStore {

    private final Map<String, List<BusinessEvent>> store;

    public UserEventStore() {
        this.store = new ConcurrentHashMap<>();
    }

    private void persistEvent(String userId, BusinessEvent event) {
        if (!store.containsKey(userId)) {
            store.put(userId, new LinkedList<>());
        }
        store.get(userId).add(event);
    }

    public void addEvents(String userId, List<BusinessEvent> appliedEvents) {
        appliedEvents.forEach($ -> persistEvent(userId, $));
    }

    public List<BusinessEvent> getEventsFor(String userId) {
        return store.getOrDefault(userId, new LinkedList<>());
    }
}
