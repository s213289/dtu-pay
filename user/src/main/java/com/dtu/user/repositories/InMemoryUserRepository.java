package com.dtu.user.repositories;

import com.dtu.user.contracts.UserRepository;
import com.dtu.user.aggregate.User;
import com.dtu.user.exception.UserAlreadyExistsException;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;

/**
 * @author MMarta Aleksandra Niewiadomska - s213040
 */
public class InMemoryUserRepository implements UserRepository {

    private UserEventStore eventStore;

    public InMemoryUserRepository(UserEventStore eventStore) {
        this.eventStore = eventStore;
    }

    @Override
    public void registerUser(User user) {
        eventStore.addEvents(user.getID().toString(), user.getAppliedEvents());
        user.clearAppliedEvents();
    }
}
