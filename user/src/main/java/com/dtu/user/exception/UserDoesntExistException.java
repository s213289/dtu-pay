package com.dtu.user.exception;

/**
 * @author Marta Ewa Lech - s212481
 */
public class UserDoesntExistException extends Exception {
    public UserDoesntExistException(String message) { super(message); }
}
