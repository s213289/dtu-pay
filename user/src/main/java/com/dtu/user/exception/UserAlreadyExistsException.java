package com.dtu.user.exception;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
public class UserAlreadyExistsException extends Exception {
    public UserAlreadyExistsException(String message) { super(message); }
}
