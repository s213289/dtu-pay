package com.dtu.user.aggregate;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationRequestedEvent;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * @author Marta Ewa Lech - s212481
 */
@Getter @Setter
public abstract class User implements Serializable {

    protected String bankAccountID;
    protected UUID ID;
    protected String firstName;
    protected String lastName;

    protected final List<BusinessEvent> appliedEvents;
    protected final Map<Class<? extends BusinessEvent>, Consumer<BusinessEvent>> handlers = new HashMap<>();


    public User() {
        this.appliedEvents = new LinkedList<>();
        registerEventHandlers();
    }

    public abstract void registerEventHandlers();

    /* Event handling */

    protected void applyEvents(List<BusinessEvent> events) throws Error {
        events.forEach(this::applyEvent);
    }

    private void applyEvent(BusinessEvent e) {
      handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e); // nwm o co chodzi z tym errorerm tutaj
    }

    private void missingHandler(BusinessEvent e) {
        throw new Error("handler for event "+e+" missing");
    }


    public void clearAppliedEvents() {
        appliedEvents.clear();
    }

    /* overrides */

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof User)) {
            return false;
        }

        User u = (User) o;

        return bankAccountID.equals(u.bankAccountID) && firstName.equals(u.firstName)
                && lastName.equals(u.lastName) && ID.equals(ID);
    }
}
