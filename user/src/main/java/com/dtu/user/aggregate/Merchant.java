package com.dtu.user.aggregate;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationResponseEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationRequestedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationResponseEvent;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * @author Marta Ewa Lech - s212481
 */
public class Merchant extends User {

    public Merchant() {
        super();
    }

    public static Merchant create(MerchantRegistrationRequestedEvent event){
            Merchant merchant = new Merchant();
            UUID id = UUID.randomUUID();
            merchant.ID = id;
            merchant.appliedEvents.add(event.setUserID(id.toString()));
            return merchant;
    }

    public static Optional<Merchant> createFromEvents(List<BusinessEvent> events) {
        try {
            Merchant merchant = new Merchant();
            merchant.applyEvents(events);
            return Optional.of(merchant);
        } catch (IllegalStateException e) {
            return Optional.empty();
        }
    }

    @Override
    public void registerEventHandlers() {
        handlers.put(MerchantRegistrationRequestedEvent.class, $ -> apply((MerchantRegistrationRequestedEvent) $));
    }

    public void apply(MerchantRegistrationRequestedEvent event){
        ID = UUID.fromString(event.getUserID());
        firstName = event.getFirstName();
        lastName = event.getLastName();
        bankAccountID = event.getBankAccountId();
    }

    public void merchantRegisterSuccess(){
        appliedEvents.add(new MerchantRegistrationResponseEvent(ID.toString()));
    }
}
