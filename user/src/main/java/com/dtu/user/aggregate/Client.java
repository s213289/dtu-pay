package com.dtu.user.aggregate;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationResponseEvent;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
public class Client extends User {

    public Client() {
        super();
    }

    public static Client create(CustomerRegistrationRequestedEvent event) {
        Client client = new Client();
        UUID id = UUID.randomUUID();
        client.ID = id;
        client.appliedEvents.add(event.setUserId(id.toString()));
        return client;
    }

    public static Optional<Client> createFromEvents(List<BusinessEvent> events) {
        try {
            Client client = new Client();
            client.applyEvents(events);
            return Optional.of(client);
        } catch (IllegalStateException e) {
            return Optional.empty();
        }
    }

    @Override
    public void registerEventHandlers() {
        handlers.put(CustomerRegistrationRequestedEvent.class, $ -> apply((CustomerRegistrationRequestedEvent) $));
    }

    public void apply(CustomerRegistrationRequestedEvent event){
        ID = UUID.fromString(event.getUserId());
        firstName = event.getFirstName();
        lastName = event.getLastName();
        bankAccountID = event.getBankAccountId();
    }

    public void clientRegisterSuccess(){

    }
}
