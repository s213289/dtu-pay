package com.dtu.user.contracts;

import com.dtu.user.aggregate.User;
import com.dtu.user.exception.UserAlreadyExistsException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
public interface UserRepository {
    public void registerUser(User user);
}
