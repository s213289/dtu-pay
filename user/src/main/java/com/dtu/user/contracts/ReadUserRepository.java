package com.dtu.user.contracts;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.user.aggregate.Client;
import com.dtu.user.aggregate.Merchant;
import com.dtu.user.aggregate.User;
import com.dtu.user.exception.UserAlreadyExistsException;
import com.dtu.user.exception.UserDoesntExistException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Marta Ewa Lech - s212481
 */
public interface ReadUserRepository {

    boolean doesUserExists(String accountId);

    UUID getClientId(UUID userId);

    UUID getMerchantId(UUID userId);

    String  getCustomerBankAccountIdByUserId(UUID userId);

    String  getMerchantBankAccountIdByUserId(UUID userId);
}
