package com.dtu.user;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerBankAccountIdRetrievedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationResponseEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdLookupFailedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantBankAccountIdRetrievedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationRequestedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationResponseEvent;
import com.dtu.user.contracts.UserRepository;
import com.dtu.user.repositories.ReadModelUserRepository;
import com.dtu.user.service.UserService;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class UserServiceTest {

    private final UserRepository userRepository = mock(UserRepository.class);
    private final ReadModelUserRepository readModelUserRepository = mock(ReadModelUserRepository.class);
    private final EventDispatcher dispatcher = mock(EventDispatcher.class);
    private final EventPublisher publisher = mock(EventPublisher.class);

    private final UserService service = new UserService(userRepository, readModelUserRepository, dispatcher, publisher);

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Test
    void registerCustomerSuccess() {

        //GIVEN
        final String testFlow = "testFlowId";
        final String firstName = "Mikolaj";
        final String lastName = "Kopernik";
        final String accountId = "account1";
        final String customerId = UUID.randomUUID().toString();

        final CustomerRegistrationRequestedEvent event =
                new CustomerRegistrationRequestedEvent(firstName, lastName, accountId);
        event.setFlowId(testFlow);

        //WHEN
        when(readModelUserRepository.getClientId(any())).thenReturn(UUID.fromString(customerId));
        service.apply(event);

        //THEN
        verify(publisher, never()).publish(new CustomerRegistrationResponseEvent(customerId));
    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    @Test
    void registerMerchantSuccess() {
        //GIVEN
        final String testFlow = "testFlowId";
        final String firstName = "Kamil";
        final String lastName = "Stoch";
        final String accountId = "account2";
        final String merchantId = UUID.randomUUID().toString();

        final MerchantRegistrationRequestedEvent event =
                new MerchantRegistrationRequestedEvent(firstName, lastName, accountId);
        event.setFlowId(testFlow);

        //WHEN
        when(readModelUserRepository.getMerchantId(any())).thenReturn(UUID.fromString(merchantId));
        service.apply(event);

        //THEN
        verify(publisher, never()).publish(new MerchantRegistrationResponseEvent(merchantId));
    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    @Test
    void getMerchantBankAccountIDSuccess() {

        //GIVEN
        final String token = "testFlowId";
        final String merchantId = UUID.randomUUID().toString();
        final String accountId = "2";
        final BigDecimal amount = BigDecimal.valueOf(100);

        final TransactionRequestedEvent transactionEvent = new TransactionRequestedEvent(token, merchantId, amount);

        //WHEN
        when(readModelUserRepository.getMerchantBankAccountIdByUserId(UUID.fromString(merchantId))).thenReturn(accountId);
        service.apply(transactionEvent);

        //THEN
        verify(publisher, never()).publish(new MerchantBankAccountIdRetrievedEvent(accountId));
    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    @Test
    void getMerchantBankAccountIDFailure() {
        //GIVEN
        final String token = "testFlowId";
        final String merchantId = UUID.randomUUID().toString();
        final BigDecimal amount = BigDecimal.valueOf(100);

        final TransactionRequestedEvent transactionEvent = new TransactionRequestedEvent(token, merchantId, amount);

       // WHEN
        when(readModelUserRepository.getMerchantBankAccountIdByUserId(any())).thenReturn(null);
        service.apply(transactionEvent);

        //THEN
        verify(publisher, never()).publish(new MerchantBankAccountIdLookupFailedEvent());
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Test
    void getCustomerBankAccountIDSuccess() {
        //GIVEN
        final String accountId = "account3";
        final String token = "testFlowId";
        final String customerId = UUID.randomUUID().toString();
        final BigDecimal amount = BigDecimal.valueOf(100);

        final TransactionRequestedEvent transactionEvent = new TransactionRequestedEvent(token, customerId, amount);

        //WHEN
        when(readModelUserRepository.getCustomerBankAccountIdByUserId(UUID.fromString(customerId))).thenReturn(accountId);
        service.apply(transactionEvent);

        //THEN
        verify(publisher, never()).publish(new CustomerBankAccountIdRetrievedEvent(accountId));
    }
}
