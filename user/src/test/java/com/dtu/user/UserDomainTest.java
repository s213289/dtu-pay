package com.dtu.user;


import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationRequestedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationRequestedEvent;
import com.dtu.user.aggregate.Client;
import com.dtu.user.aggregate.Merchant;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class UserDomainTest {

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Test
    void createCustomerSuccess() {
        //GIVEN
        String firstName ="Robert";
        String lastName = "Lewandowski";
        String accountId = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();
        CustomerRegistrationRequestedEvent registerCustomerEvent = new CustomerRegistrationRequestedEvent(firstName,
                lastName, accountId).setUserId(userId);
        Optional<Client> c = Client.createFromEvents(List.of(registerCustomerEvent));

        //THEN
        assertTrue(c.isPresent());
        assertEquals(accountId, c.get().getBankAccountID());
        assertEquals(firstName, c.get().getFirstName());
        assertEquals(lastName, c.get().getLastName());
    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    @Test
    void createMerchantSuccess() {
        //GIVEN
        String firstName ="Anna";
        String lastName = "Lewandowska";
        String accountId = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();
        MerchantRegistrationRequestedEvent registerMerchantEvent = new MerchantRegistrationRequestedEvent(firstName,
                lastName, accountId).setUserID(userId);
        Optional<Merchant> m = Merchant.createFromEvents(List.of(registerMerchantEvent));

        //THEN
        assertTrue(m.isPresent());
        assertEquals(accountId, m.get().getBankAccountID());
        assertEquals(firstName, m.get().getFirstName());
        assertEquals(lastName, m.get().getLastName());
    }

}
