package com.dtu.token;
import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.token.*;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.token.persistence.InMemoryTokenRepository;
import com.dtu.token.persistence.InMemoryTokenRepositoryAccessor;
import com.dtu.token.persistence.TokenEventStore;
import com.dtu.token.service.TokenService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.*;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
public class TokenServiceTest {
    private final EventDispatcher dispatcher = mock(EventDispatcher.class);
    private final EventPublisher publisher = mock(EventPublisher.class);
    private final TokenEventStore store = new TokenEventStore(dispatcher, publisher);
    private final InMemoryTokenRepositoryAccessor accessor = new InMemoryTokenRepositoryAccessor(store);
    private final InMemoryTokenRepository repository = new InMemoryTokenRepository(store);
    private final TokenService tokenService = new TokenService(accessor, repository, publisher, dispatcher);


    @Test
    void generateAndCountTokens() {
        UUID customerId = UUID.randomUUID();
        int amount = 5;
        tokenService.generateTokens(new TokenGenerateEvent().setCustomerId(customerId).setAmount(amount));
        assertEquals(5, accessor.getValidTokenCountForCustomer(customerId));
    }

    @Test
    void generateTokensFailsWhenValidTokensExist() {
        UUID customerId = UUID.randomUUID();
        int amount = 5;
        tokenService.generateTokens(new TokenGenerateEvent().setCustomerId(customerId).setAmount(amount));
        tokenService.generateTokens(new TokenGenerateEvent().setCustomerId(customerId).setAmount(amount));
        ArgumentCaptor<TokenGenerationFailedEvent> argumentCaptor = ArgumentCaptor.forClass(TokenGenerationFailedEvent.class);
        verify(publisher, times(2)).publish(argumentCaptor.capture());
        assertEquals("The customer has more than 1 unused token left. customerId = " + customerId, argumentCaptor.getValue().getMessage());
    }

    @Test
    void generateTokensFailsWhenWrongNumberSpecified() {
        UUID customerId = UUID.randomUUID();
        int amount = 7;
        tokenService.generateTokens(new TokenGenerateEvent().setCustomerId(customerId).setAmount(amount));
        ArgumentCaptor<TokenGenerationFailedEvent> argumentCaptor = ArgumentCaptor.forClass(TokenGenerationFailedEvent.class);
        verify(publisher, times(1)).publish(argumentCaptor.capture());
        assertEquals("Incorrect amount of tokens requested. amount = " + amount, argumentCaptor.getValue().getMessage());
    }

    @Test
    void consumeTokenSuccess() {
        UUID customerId = UUID.randomUUID();
        int amount = 5;
        tokenService.generateTokens(new TokenGenerateEvent().setCustomerId(customerId).setAmount(amount));
        ArgumentCaptor<TokenGenerationSuccessEvent> argumentCaptor = ArgumentCaptor.forClass(TokenGenerationSuccessEvent.class);
        verify(publisher, times(1)).publish(argumentCaptor.capture());
        assertFalse(argumentCaptor.getValue().getTokens().isEmpty());

        TransactionRequestedEvent request = new TransactionRequestedEvent();
        request.setToken(argumentCaptor.getValue().getTokens().get(0).toString());
        tokenService.consumeToken(request);
        ArgumentCaptor<TokenLookupSuccessEvent> successCaptor = ArgumentCaptor.forClass(TokenLookupSuccessEvent.class);
        verify(publisher, times(2)).publish(successCaptor.capture());
        assertEquals(customerId, argumentCaptor.getValue().getCustomerId());
    }

    @Test
    void consumeTokenFail() {
        TransactionRequestedEvent request = new TransactionRequestedEvent();
        UUID randomTokenId = UUID.randomUUID();
        request.setToken(randomTokenId.toString());
        tokenService.consumeToken(request);
        ArgumentCaptor<TokenLookupFailedEvent> failureCaptor = ArgumentCaptor.forClass(TokenLookupFailedEvent.class);
        verify(publisher, times(1)).publish(failureCaptor.capture());
        assertEquals("No token with the specified id is active. tokenId = " + randomTokenId, failureCaptor.getValue().getMessage());
    }

    @Test
    void consumeSameTokenTwiceFail() {
        UUID customerId = UUID.randomUUID();
        int amount = 5;
        tokenService.generateTokens(new TokenGenerateEvent().setCustomerId(customerId).setAmount(amount));
        ArgumentCaptor<TokenGenerationSuccessEvent> argumentCaptor = ArgumentCaptor.forClass(TokenGenerationSuccessEvent.class);
        verify(publisher, times(1)).publish(argumentCaptor.capture());
        assertFalse(argumentCaptor.getValue().getTokens().isEmpty());

        TransactionRequestedEvent request = new TransactionRequestedEvent();
        request.setToken(argumentCaptor.getValue().getTokens().get(0).toString());
        tokenService.consumeToken(request);
        ArgumentCaptor<TokenLookupSuccessEvent> successCaptor = ArgumentCaptor.forClass(TokenLookupSuccessEvent.class);
        verify(publisher, times(2)).publish(successCaptor.capture());
        assertEquals(customerId, argumentCaptor.getValue().getCustomerId());
        tokenService.consumeToken(request);

        ArgumentCaptor<TokenLookupFailedEvent> failureCaptor = ArgumentCaptor.forClass(TokenLookupFailedEvent.class);
        verify(publisher, times(3)).publish(failureCaptor.capture());
        assertEquals("No token with the specified id is active. tokenId = " + argumentCaptor.getValue().getTokens().get(0), failureCaptor.getValue().getMessage());
    }
}
