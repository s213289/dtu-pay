package com.dtu.token.model;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.base.domain.model.event.token.TokenGenerateEvent;
import com.dtu.base.domain.model.event.token.TokenLookupSuccessEvent;
import com.dtu.token.events.TokenGeneratedEvent;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
@Getter
@Setter
@EqualsAndHashCode
@Slf4j
public class Token implements Serializable {
    private UUID tokenId;
    private UUID customerId;
    private boolean used;

    protected final List<BusinessEvent> appliedEvents;
    protected final Map<Class<? extends BusinessEvent>, Consumer<BusinessEvent>> handlers = new HashMap<>();

    public Token() {
        this.appliedEvents = new LinkedList<>();
        registerEventHandlers();
    }
    public static Token generate(TokenGenerateEvent event) {
        Token token = new Token();
        token.customerId = event.getCustomerId();
        token.tokenId = UUID.randomUUID();
        token.used = false;
        token.appliedEvents.add(event);
        token.appliedEvents.add(new TokenGeneratedEvent().setToken(token));
        return token;
    }

    public static Token markAsUsed(Token token, TokenLookupSuccessEvent event) {
        token.appliedEvents.add(event);
        return token;
    }

    public void registerEventHandlers() {
        handlers.put(TokenGeneratedEvent.class, $ -> apply((TokenGeneratedEvent) $));
        handlers.put(TokenLookupSuccessEvent.class,
                $ -> apply((TokenLookupSuccessEvent) $));
        handlers.put(TokenGenerateEvent.class,
                $ -> apply((TokenGenerateEvent) $));
    }

    /* Event handling */

    public static Optional<Token> createFromEvents(List<BusinessEvent> events) {
        log.info("Token: createFromEvents");
        try {
            Token token = new Token();
            token.applyEvents(events);
            return Optional.of(token);
        } catch (IllegalStateException e) {
            return Optional.empty();
        }
    }

    protected void applyEvents(List<BusinessEvent> events) throws Error {
        log.info("Token: applyEvents");
        events.forEach(this::applyEvent);
    }

    private void applyEvent(BusinessEvent e) {
        log.info("Token: applyEvent");
        handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e);
    }

    private void missingHandler(BusinessEvent e) {
        throw new Error("handler for event "+e+" missing");
    }

    public void apply(TokenGeneratedEvent event) {
        log.info("Token: TokenGeneratedEvent");
        this.customerId = event.getToken().getCustomerId();
        this.tokenId = event.getToken().getTokenId();
    }

    public void apply(TokenLookupSuccessEvent event) {
        log.info("Token: TokenLookupSuccessEvent");
        this.customerId = event.getCustomerId();
        this.tokenId = event.getTokenId();
        this.used = true;
    }

    public void apply(TokenGenerateEvent event) {
        this.customerId = event.getCustomerId();
        log.info("" + event);
    }

    public void clearAppliedEvents() {
        appliedEvents.clear();
    }


}
