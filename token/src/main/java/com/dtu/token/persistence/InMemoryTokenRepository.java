package com.dtu.token.persistence;

import com.dtu.token.model.Token;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
public class InMemoryTokenRepository {
    private TokenEventStore tokenEventStore;

    public InMemoryTokenRepository(TokenEventStore tokenEventStore) {
        this.tokenEventStore = tokenEventStore;
    }

    public void addToken(Token token) {
        tokenEventStore.addEvents(token.getTokenId(), token.getAppliedEvents());
        token.clearAppliedEvents();
    }


}
