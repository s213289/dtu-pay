package com.dtu.token.persistence;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.base.domain.model.event.token.TokenLookupSuccessEvent;
import com.dtu.token.events.TokenGeneratedEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
@Slf4j
public class TokenEventStore {
  private final Map<UUID, List<BusinessEvent>> store;
  private final EventPublisher publisher;

  public TokenEventStore(EventDispatcher dispatcher, EventPublisher publisher) {
    this.store = new ConcurrentHashMap<>();
    this.publisher = publisher;

    dispatcher.register(TokenGeneratedEvent.class,
            $ -> apply((TokenGeneratedEvent) $));
    dispatcher.register(TokenLookupSuccessEvent.class,
            $ -> apply((TokenLookupSuccessEvent) $));
  }

  private void persistEvent(UUID tokenId, BusinessEvent event) {
    log.info("TokenEventStore: persistEvent");
    if (!store.containsKey(tokenId)) {
      store.put(tokenId, new LinkedList<>());
    }
    store.get(tokenId).add(event);
  }

  public void addEvent(UUID tokenId, BusinessEvent event) {
    log.info("TokenEventStore: addEvent");
    persistEvent(tokenId, event);
  }

  public void addEvents(UUID tokenId, List<BusinessEvent> appliedEvents) {
    log.info("TokenEventStore: addEvents");
    appliedEvents.forEach($ -> addEvent(tokenId, $));
  }

  public List<BusinessEvent> getEventsFor(UUID tokenId) {
    return store.getOrDefault(tokenId, new LinkedList<>());
  }

  public List<UUID> getAllTokenIds() {
    return new ArrayList<>(store.keySet());
  }

  private void apply(TokenGeneratedEvent event) {
    persistEvent(event.getToken().getTokenId(), event);
  }

  private void apply(TokenLookupSuccessEvent event) {persistEvent(event.getTokenId(), event);}
}
