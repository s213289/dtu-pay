package com.dtu.token.persistence;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.token.model.Token;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
@ApplicationScoped
public class InMemoryTokenRepositoryAccessor {

    TokenEventStore tokenEventStore;

    public InMemoryTokenRepositoryAccessor(TokenEventStore tokenEventStore) {
        this.tokenEventStore = tokenEventStore;
    }

    public Token getTokenFromTokenId(UUID tokenId) {
        List<BusinessEvent> events = tokenEventStore.getEventsFor(tokenId);
        if (events.isEmpty()) {
            return null;
        } else {
            Token token = Token.createFromEvents(events).orElseThrow();
            return token.isUsed() ? null : token;
        }
    }

    public int getValidTokenCountForCustomer(UUID customerId) {
        List<UUID> tokens = tokenEventStore.getAllTokenIds();
        return (int) tokens.stream()
                .map(token -> tokenEventStore.getEventsFor(token))
                .map(events -> Token.createFromEvents(events).orElseThrow())
                .filter(token -> token.getCustomerId().equals(customerId))
                .filter(token -> !token.isUsed()).count();
    }
}
