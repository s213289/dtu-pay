package com.dtu.token.events;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.token.model.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
@Getter
@Setter
public class TokenGeneratedEvent extends BusinessEvent {
    private Token token;
}
