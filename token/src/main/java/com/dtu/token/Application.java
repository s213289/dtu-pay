package com.dtu.token;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;

import com.dtu.rabbitmq.receiver.RabbitMqEventDispatcher;
import com.dtu.rabbitmq.sender.RabbitMqEventPublisher;

import com.dtu.token.persistence.TokenEventStore;
import com.dtu.token.persistence.InMemoryTokenRepository;
import com.dtu.token.persistence.InMemoryTokenRepositoryAccessor;

import com.dtu.token.service.TokenService;
import io.quarkus.runtime.Startup;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
public class Application {
  @Startup @ApplicationScoped
  public TokenService tokenService(InMemoryTokenRepositoryAccessor accessor, InMemoryTokenRepository repository, EventPublisher publisher, EventDispatcher dispatcher) {
    return new TokenService(accessor, repository, publisher, dispatcher);
  }

  @ApplicationScoped
  public TokenEventStore eventStore(EventDispatcher dispatcher, EventPublisher publisher) {
    return new TokenEventStore(dispatcher, publisher);
  }

  @Startup
  @ApplicationScoped
  public InMemoryTokenRepository tokenRepository(TokenEventStore tokenEventStore) {
    return new InMemoryTokenRepository(tokenEventStore);
  }

  @ApplicationScoped
  public EventPublisher eventPublisher() {
    return new RabbitMqEventPublisher();
  }

  @ApplicationScoped
  public EventDispatcher eventDispatcher() {
    return new RabbitMqEventDispatcher();
  }
}
