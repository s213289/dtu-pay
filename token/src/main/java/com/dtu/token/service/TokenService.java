package com.dtu.token.service;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.token.TokenGenerateEvent;
import com.dtu.base.domain.model.event.token.TokenGenerationFailedEvent;
import com.dtu.base.domain.model.event.token.TokenGenerationSuccessEvent;
import com.dtu.base.domain.model.event.token.TokenLookupFailedEvent;
import com.dtu.base.domain.model.event.token.TokenLookupSuccessEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.token.model.Token;
import com.dtu.token.persistence.InMemoryTokenRepository;
import com.dtu.token.persistence.InMemoryTokenRepositoryAccessor;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
@ApplicationScoped
@Slf4j
public class TokenService {

    private final InMemoryTokenRepositoryAccessor accessor;
    private final EventPublisher publisher;
    private final InMemoryTokenRepository repository;

    public TokenService(InMemoryTokenRepositoryAccessor accessor, InMemoryTokenRepository repository, EventPublisher publisher, EventDispatcher dispatcher) {
        this.repository = repository;
        this.accessor = accessor;
        this.publisher = publisher;

        dispatcher.register(TokenGenerateEvent.class,
                $ -> apply((TokenGenerateEvent) $));
        dispatcher.register(TransactionRequestedEvent.class,
                $ -> apply((TransactionRequestedEvent) $));
    }
    public void generateTokens(TokenGenerateEvent event) {
        int amount = event.getAmount();
        UUID customerId = event.getCustomerId();
        log.info("TokenService: Generate tokens");
        if (amount < 1 || amount > 5) {
            TokenGenerationFailedEvent failedEvent = new TokenGenerationFailedEvent();
            failedEvent.setMessage("Incorrect amount of tokens requested. amount = " + amount);
            failedEvent.setFlowId(event.getFlowId());
            publisher.publish(failedEvent);
            return;
        }
        if (accessor.getValidTokenCountForCustomer(customerId) > 1) {
            TokenGenerationFailedEvent failedEvent = new TokenGenerationFailedEvent();
            failedEvent.setMessage("The customer has more than 1 unused token left. customerId = " + customerId);
            failedEvent.setFlowId(event.getFlowId());
            publisher.publish(failedEvent);
            return;
        }
        List<Token> generatedTokens = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            Token token = Token.generate(event);
            repository.addToken(token);
            generatedTokens.add(token);
        }

        TokenGenerationSuccessEvent successEvent = new TokenGenerationSuccessEvent();
        successEvent.setFlowId(event.getFlowId());
        successEvent.setCustomerId(customerId);
        successEvent.setTokens(generatedTokens.stream().map(Token::getTokenId).collect(Collectors.toList()));
        log.info("TokenService: Publish TokenGenerationSuccessEvent");
        publisher.publish(successEvent);
    }

    public void consumeToken(TransactionRequestedEvent requestEvent) {
        UUID tokenId = UUID.fromString(requestEvent.getToken());
        Token token = accessor.getTokenFromTokenId(tokenId);
        if (token == null) {
            publisher.publish(new TokenLookupFailedEvent()
                    .setMessage("No token with the specified id is active. tokenId = " + tokenId).setToken(tokenId).setFlowId(requestEvent.getFlowId()));
            return;
        }
        TokenLookupSuccessEvent event = new TokenLookupSuccessEvent();
        event.setTokenId(token.getTokenId());
        event.setCustomerId(token.getCustomerId());
        event.setFlowId(requestEvent.getFlowId());
        repository.addToken(Token.markAsUsed(token, event));
        publisher.publish(event);
    }

    private void apply(TokenGenerateEvent event) {
        log.info("TokenService: apply TokenGenerateEvent");
        generateTokens(event);
    }

    private void apply(TransactionRequestedEvent event) {
        log.info("TokenService: apply TransactionRequestedEvent");
        consumeToken(event);
    }
}
