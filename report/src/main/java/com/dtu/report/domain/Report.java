package com.dtu.report.domain;

import com.dtu.base.domain.model.event.transaction.TransactionFinishedEvent;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jmolecules.ddd.annotation.AggregateRoot;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@AggregateRoot
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class Report {
  private String token;
  private String customerId;
  private String merchantId;
  private String customerBankAccount;
  private String merchantBankAccount;
  private BigDecimal amount;
  private LocalDateTime timestamp;

  public Report(TransactionFinishedEvent event) {
    this.token = event.getToken();
    this.customerId = event.getCustomerId();
    this.merchantId = event.getMerchantId();
    this.customerBankAccount = event.getCustomerAccount();
    this.merchantBankAccount = event.getMerchantAccount();
    this.amount = event.getAmount();
    this.timestamp = event.getTimestamp();
  }

  @AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
  public static class CustomerReport {
    private String token;
    private String merchantId;
    private BigDecimal amount;
    private LocalDateTime timestamp;

    public CustomerReport(TransactionFinishedEvent event) {
      this.token = event.getToken();
      this.merchantId = event.getMerchantId();
      this.amount = event.getAmount();
      this.timestamp = event.getTimestamp();
    }
  }

  @AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
  public static class MerchantReport {
    private String token;
    private BigDecimal amount;
    private LocalDateTime timestamp;

    public MerchantReport(TransactionFinishedEvent event) {
      this.token = event.getToken();
      this.amount = event.getAmount();
      this.timestamp = event.getTimestamp();
    }
  }
}
