package com.dtu.report.domain.repositories;

import com.dtu.report.domain.Report;
import java.util.Set;
import org.jmolecules.ddd.annotation.Repository;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
@Repository
public interface ReportReadModel {
  Set<Report> getManagerReports();

  Set<Report.CustomerReport> getCustomerReportsById(String id);

  Set<Report.MerchantReport> getMerchantReportsById(String id);
}
