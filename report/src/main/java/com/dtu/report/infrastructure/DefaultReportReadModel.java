package com.dtu.report.infrastructure;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.model.event.transaction.TransactionFinishedEvent;
import com.dtu.report.domain.Report;
import com.dtu.report.domain.repositories.ReportReadModel;
import io.vertx.core.impl.ConcurrentHashSet;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
public class DefaultReportReadModel implements ReportReadModel {
  private final Set<Report> managerReports;
  private final Map<String, Set<Report.CustomerReport>> customerReports;
  private final Map<String, Set<Report.MerchantReport>> merchantReports;

  public DefaultReportReadModel(EventDispatcher dispatcher) {
    this.managerReports = new ConcurrentHashSet<>();
    this.customerReports = new ConcurrentHashMap<>();
    this.merchantReports = new ConcurrentHashMap<>();

    dispatcher.register(TransactionFinishedEvent.class, $ -> apply((TransactionFinishedEvent) $));
  }

  private void apply(TransactionFinishedEvent event) {
    managerReports.add(new Report(event));
    Set<Report.CustomerReport> currentCustomerReports =
        customerReports.getOrDefault(event.getCustomerId(), new HashSet<>());
    currentCustomerReports.add(new Report.CustomerReport(event));
    customerReports.put(event.getCustomerId(), currentCustomerReports);
    Set<Report.MerchantReport> currentMerchantReports =
        merchantReports.getOrDefault(event.getMerchantId(), new HashSet<>());
    currentMerchantReports.add(new Report.MerchantReport(event));
    merchantReports.put(event.getMerchantId(), currentMerchantReports);
  }

  @Override public Set<Report> getManagerReports() {
    return managerReports;
  }

  @Override public Set<Report.CustomerReport> getCustomerReportsById(String id) {
    return customerReports.getOrDefault(id, new HashSet<>());
  }

  @Override public Set<Report.MerchantReport> getMerchantReportsById(String id) {
    return merchantReports.getOrDefault(id, new HashSet<>());
  }
}
