package com.dtu.report.application;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.rabbitmq.receiver.RabbitMqEventDispatcher;
import com.dtu.rabbitmq.sender.RabbitMqEventPublisher;
import com.dtu.report.domain.repositories.ReportReadModel;
import com.dtu.report.infrastructure.DefaultReportReadModel;
import io.quarkus.runtime.Startup;
import javax.enterprise.context.ApplicationScoped;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
public class Application {
  @Startup @ApplicationScoped
  public ReportService reportService(ReportReadModel readModel, EventDispatcher dispatcher, EventPublisher publisher){
    return new ReportService(readModel, dispatcher, publisher);
  }

  @Startup @ApplicationScoped
  public ReportReadModel readModel(EventDispatcher dispatcher) {
    return new DefaultReportReadModel(dispatcher);
  }

  @ApplicationScoped
  public EventPublisher eventPublisher() {
    return new RabbitMqEventPublisher();
  }

  @ApplicationScoped
  public EventDispatcher eventDispatcher() {
    return new RabbitMqEventDispatcher();
  }
}
