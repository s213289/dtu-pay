package com.dtu.report.application;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.report.CustomerReportGeneratedEvent;
import com.dtu.base.domain.model.event.report.GenerateCustomerReportEvent;
import com.dtu.base.domain.model.event.report.GenerateManagerReportEvent;
import com.dtu.base.domain.model.event.report.GenerateMerchantReportEvent;
import com.dtu.base.domain.model.event.report.ManagerReportGeneratedEvent;
import com.dtu.base.domain.model.event.report.MerchantReportGeneratedEvent;
import com.dtu.report.domain.repositories.ReportReadModel;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.jmolecules.ddd.annotation.Service;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
@Service
@Slf4j
public class ReportService {
  private final ReportReadModel readModel;
  private final EventDispatcher dispatcher;
  private final EventPublisher publisher;

  public ReportService(ReportReadModel readModel, EventDispatcher dispatcher,
      EventPublisher publisher) {
    this.readModel = readModel;
    this.dispatcher = dispatcher;
    this.publisher = publisher;

    dispatcher.register(GenerateManagerReportEvent.class,
        $ -> apply((GenerateManagerReportEvent) $));
    dispatcher.register(GenerateMerchantReportEvent.class,
        $ -> apply((GenerateMerchantReportEvent) $));
    dispatcher.register(GenerateCustomerReportEvent.class,
        $ -> apply((GenerateCustomerReportEvent) $));
  }

  private void apply(GenerateManagerReportEvent event) {
    Set<ManagerReportGeneratedEvent.Report> reports =
        readModel.getManagerReports()
            .stream()
            .map($ -> new ManagerReportGeneratedEvent.Report(
                $.getToken(),
                $.getCustomerId(),
                $.getMerchantId(),
                $.getCustomerBankAccount(),
                $.getCustomerBankAccount(),
                $.getAmount(),
                $.getTimestamp()
            ))
            .collect(
                Collectors.toSet()
            );
    publisher.publish(new ManagerReportGeneratedEvent(reports).setFlowId(event.getFlowId()));
  }

  private void apply(GenerateMerchantReportEvent event) {
    Set<MerchantReportGeneratedEvent.MerchantReport> reports =
        readModel.getMerchantReportsById(event.getMerchantId())
            .stream()
            .map($ -> new MerchantReportGeneratedEvent.MerchantReport(
                $.getToken(),
                $.getAmount(),
                $.getTimestamp()
            ))
            .collect(
                Collectors.toSet()
            );
    publisher.publish(new MerchantReportGeneratedEvent(reports).setFlowId(event.getFlowId()));
  }

  private void apply(GenerateCustomerReportEvent event) {
    Set<CustomerReportGeneratedEvent.CustomerReport> reports =
        readModel.getCustomerReportsById(event.getCustomerId())
            .stream()
            .map($ -> new CustomerReportGeneratedEvent.CustomerReport(
                $.getToken(),
                $.getMerchantId(),
                $.getAmount(),
                $.getTimestamp()
            ))
            .collect(
                Collectors.toSet()
            );
    publisher.publish(new CustomerReportGeneratedEvent(reports).setFlowId(event.getFlowId()));
  }
}
