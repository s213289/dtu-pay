package com.dtu.user;

import com.dtu.domain.BankService;
import com.dtu.domain.BankServiceException_Exception;
import com.dtu.domain.BankServiceService;
import com.dtu.domain.User;
import com.dtu.domain.user.UserRegisterRequest;
import com.dtu.domain.user.UserRegisterResponse;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UserSteps {

    BankService bank = new BankServiceService().getBankServicePort();
    WebTarget baseUrl;

    private User customer = new User();
    private User merchant = new User();
    private String customerBankAccountId;
    private String merchantBankAccountId;
    private UserRegisterResponse customerResponse;
    private UserRegisterResponse merchantResponse;

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Before
    public void initialize(){
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
    }


    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Given("a customer {string} {string} with CPR {string} has a bank account with balance {int} dkk")
    public void createCustomerBankAccount(String firstName, String lastName, String cpr, int balance) throws
        BankServiceException_Exception {
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setCprNumber(cpr);
        customerBankAccountId = bank.createAccountWithBalance(customer, BigDecimal.valueOf(balance));
    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    @Given("a merchant {string} {string} with CPR {string} has a bank account with balance {int} dkk")
    public void createMerchantBankAccount(String firstName, String lastName, String cpr, int balance) throws BankServiceException_Exception {
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchant.setCprNumber(cpr);
        merchantBankAccountId = bank.createAccountWithBalance(merchant, BigDecimal.valueOf(balance));
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @When("the customer registers at DTU-Pay")
    public void registerCustomerAtDTU(){
        UserRegisterRequest request = new UserRegisterRequest(customer.getFirstName(), customer.getLastName(), customerBankAccountId);
        customerResponse = baseUrl.path("customer")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), UserRegisterResponse.class);

    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    @When("the merchant registers at DTU-Pay")
    public void registerMerchantAtDTU(){
        UserRegisterRequest request = new UserRegisterRequest(merchant.getFirstName(), merchant.getLastName(), merchantBankAccountId);
        merchantResponse = baseUrl.path("merchant")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(request, MediaType.APPLICATION_JSON), UserRegisterResponse.class);

    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Then("answer contains the id value for a customer")
    public void checkCustomerRegistrationResponse(){
        assertNotNull(customerResponse.getUserId());
    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    @Then("answer contains the id value for a merchant")
    public void checkMerchantRegistrationResponse(){
        assertNotNull(merchantResponse.getUserId());
    }

    /**
     * @author Marta Ewa Lech - s212481
     */
    @After
    public void retireAccounts() throws BankServiceException_Exception {
        if(customerBankAccountId != null){
            bank.retireAccount(customerBankAccountId);
        }
        if(merchantBankAccountId != null){
            bank.retireAccount(merchantBankAccountId);
        }
    }
}
