package com.dtu;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import org.junit.runner.RunWith;

/**
 * @author Aleksander Jarmołkowicz - s213289, copied from Hubert Baumeister project
 */
@RunWith(Cucumber.class)
@CucumberOptions(
		plugin="summary",
		snippets = SnippetType.CAMELCASE,
		features="features")
public class CucumberTest {
}