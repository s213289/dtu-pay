package com.dtu.token;

import com.dtu.domain.BankService;
import com.dtu.domain.BankServiceException_Exception;
import com.dtu.domain.BankServiceService;
import com.dtu.domain.User;
import com.dtu.domain.token.TokenRequestRequest;
import com.dtu.domain.token.TokenRequestResponse;
import com.dtu.domain.user.UserRegisterRequest;
import com.dtu.domain.user.UserRegisterResponse;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TokenSteps {
  private final BankService bank = new BankServiceService().getBankServicePort();
  private WebTarget baseUrl;

  private User customer = new User();

  // -- User specific
  private String customerBankAccountId, customerId;

  // -- Token specific
  private List<String> tokens;
  private String message;

  /**
   * @author Grzegorz Jacek Kot - s200358
   */
  @Before
  public void initialize() {
    Client client = ClientBuilder.newClient();
    baseUrl = client.target("http://localhost:8080/");
    tokens = new LinkedList<>();
  }

  /**
   * @author Grzegorz Jacek Kot - s200358
   */
  @Given("a customer {string} {string} with CPR {string} with bank account balance {int} dkk")
  public void aCustomerWithCPRWithBankAccountBalanceDkk(String firstName, String lastName,
      String cpr, int balance)
      throws BankServiceException_Exception {
    customer.setFirstName(firstName);
    customer.setLastName(lastName);
    customer.setCprNumber(cpr);
    customerBankAccountId = bank.createAccountWithBalance(customer, BigDecimal.valueOf(balance));
  }

  /**
   * @author Grzegorz Jacek Kot - s200358
   */
  @And("the customer is registered in DTU Pay")
  public void theCustomerIsRegisteredInDTUPay() {
    UserRegisterRequest customerRequest =
        new UserRegisterRequest(customer.getFirstName(), customer.getLastName(),
            customerBankAccountId);

    customerId = baseUrl.path("customer")
        .request(MediaType.APPLICATION_JSON)
        .post(Entity.entity(customerRequest, MediaType.APPLICATION_JSON),
            UserRegisterResponse.class).getUserId();

    assertNotNull(customerId);
  }

  /**
   * @author Grzegorz Jacek Kot - s200358
   */
  @When("the customer requests valid amount of {int} tokens")
  public void theCustomerRequestsValidAmountOfTokens(int amount) {
    TokenRequestRequest request = new TokenRequestRequest(customerId, amount);

    List<String> generatedTokens = baseUrl.path("customer")
        .path("token")
        .request(MediaType.APPLICATION_JSON)
        .post(Entity.entity(request, MediaType.APPLICATION_JSON), TokenRequestResponse.class)
        .getTokens();

    if (generatedTokens != null) {
      tokens.addAll(generatedTokens);
    }
  }

  /**
   * @author Marta Ewa Lech - s212481
   */
  @Then("the customer has {int} unused tokens")
  public void theCustomerHasUnusedTokens(int amount) {
    assertEquals(amount, tokens.size());
  }

  /**
   * @author Marta Ewa Lech - s212481
   */
  @When("the customer requests invalid amount of {int} tokens")
  public void theCustomerRequestsInvalidAmountOfTokens(int amount) {
    TokenRequestRequest request = new TokenRequestRequest(customerId, amount);

    message = baseUrl.path("customer")
        .path("token")
        .request(MediaType.APPLICATION_JSON)
        .post(Entity.entity(request, MediaType.APPLICATION_JSON), TokenRequestResponse.class)
        .getMessage();
  }

  /**
   * @author Marta Aleksandra Niewiadomska - s213040
   */
  @Then("the customer received an error meassge")
  public void theCustomerReceivedAnErrorMeassge() {
    assertTrue(message.contains("Token generation failed"));
  }

  /**
   * @author Marta Ewa Lech - s212481
   */
  @After
  public void retireAccount() throws BankServiceException_Exception {
    if (customerBankAccountId != null) {
      bank.retireAccount(customerBankAccountId);
    }
    tokens = new LinkedList<>();
    message = "";
  }
}
