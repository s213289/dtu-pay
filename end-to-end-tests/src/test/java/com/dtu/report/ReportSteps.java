package com.dtu.report;

import com.dtu.domain.BankService;
import com.dtu.domain.BankServiceException_Exception;
import com.dtu.domain.BankServiceService;
import com.dtu.domain.User;
import com.dtu.domain.report.CustomerReportResponse;
import com.dtu.domain.report.ManagerReportResponse;
import com.dtu.domain.report.MerchantReportResponse;
import com.dtu.domain.token.TokenRequestRequest;
import com.dtu.domain.token.TokenRequestResponse;
import com.dtu.domain.transaction.TransactionRequest;
import com.dtu.domain.transaction.TransactionResponse;
import com.dtu.domain.user.UserRegisterRequest;
import com.dtu.domain.user.UserRegisterResponse;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.math.BigDecimal;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.junit.Assert;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *  @author Bartłomiej Józef Otto - s203042
 */
public class ReportSteps {

    BankService bank = new BankServiceService().getBankServicePort();
    WebTarget baseUrl;

    private User customer = new User();
    private User merchant = new User();
    private String customerBankAccountId, customerId;
    private String merchantBankAccountId, merchantId;
    List<String> tokens;
    int usedTokens = 0;
    private CustomerReportResponse customerReport;
    private MerchantReportResponse merchantReport;
    private ManagerReportResponse managerReport;


    @Before
    public void initialize(){
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
    }

    @Given("a customer {string} {string} with CPR number {string} and account balance {int} dkk registered in bank and dtu pay")
    public void registerCustomer(String firstName, String lastName, String cpr, int balance) throws
        BankServiceException_Exception {
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setCprNumber(cpr);
        customerBankAccountId = bank.createAccountWithBalance(customer, BigDecimal.valueOf(balance));
        assertNotNull(customerBankAccountId);

        // register at DTU
        UserRegisterRequest request = new UserRegisterRequest(customer.getFirstName(), customer.getLastName(), customerBankAccountId);
        customerId= baseUrl.path("customer")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), UserRegisterResponse.class).getUserId();
        assertNotNull(customerId);
    }

    @Given("a merchant {string} {string} with CPR {string} has a bank account with balance {int} dkk and registered in bank and dtu pay")
    public void registerMerchant(String firstName, String lastName, String cpr, int balance) throws BankServiceException_Exception{
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchant.setCprNumber(cpr);
        merchantBankAccountId = bank.createAccountWithBalance(merchant, BigDecimal.valueOf(balance));
        assertNotNull(merchantBankAccountId);

        //register at DTU
        UserRegisterRequest request = new UserRegisterRequest(merchant.getFirstName(), merchant.getLastName(), merchantBankAccountId);
        merchantId = baseUrl.path("merchant")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), UserRegisterResponse.class).getUserId();
        assertNotNull(merchantId);
    }

    @Given("a customer requests {int} tokens successfully")
    public void customerRequestTokens(int tokenCount){
        TokenRequestRequest request = new TokenRequestRequest(customerId, tokenCount);

        tokens = baseUrl.path("customer")
            .path("token")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), TokenRequestResponse.class).getTokens();

        Assert.assertEquals(tokenCount, tokens.size());
    }

    @Given(("a merchant request a payment of {int} dkk successfully"))
    public void merchantRequestPayment(int amount){
        TransactionRequest request = new TransactionRequest(tokens.get(usedTokens), merchantId, BigDecimal.valueOf(amount));
        TransactionResponse transactionResponse = baseUrl.path("merchant").path("transaction")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), TransactionResponse.class);
        assertTrue(transactionResponse.isStatus());
        usedTokens++;
    }

    @When("a customer generates a report")
    public void customerGeneratesReport(){
        customerReport =baseUrl.path("customer")
            .path("report")
            .path(customerId)
            .request(MediaType.APPLICATION_JSON)
            .get(CustomerReportResponse.class);
    }

    @When("a merchant generates a report")
    public void merchantGeneratesReport(){
        merchantReport =baseUrl.path("merchant")
            .path("report")
            .path(merchantId)
            .request(MediaType.APPLICATION_JSON)
            .get(MerchantReportResponse.class);
    }

    @When("a manager generates a report")
    public void managerGeneratesReport(){
        managerReport = baseUrl.path("manager")
            .path("report")
            .request(MediaType.APPLICATION_JSON)
            .get(ManagerReportResponse.class);
    }

    @Then("customer report consists of {int} items")
    public void checkCustomerReportContent(int itemsCount){
        assertEquals(itemsCount, customerReport.getReports().size());
    }

    @Then("merchant report consists of {int} items")
    public void checkMerchantReportContent(int itemsCount){
        assertEquals(itemsCount, merchantReport.getReports().size());
    }

    @Then("manager report consists of {int} items")
    public void checkManagerReportContent(int itemsCount){
        assertEquals(itemsCount, managerReport.getReports().size());
    }

    @After
    public void retireAccounts() throws BankServiceException_Exception {
        if(customerBankAccountId != null){
            bank.retireAccount(customerBankAccountId);
        }
        if(merchantBankAccountId != null){
            bank.retireAccount(merchantBankAccountId);
        }
    }
}
