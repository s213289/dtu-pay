package com.dtu.domain.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class UserRegisterRequest {
  private String firstName;
  private String lastName;
  private String bankAccountId;
}
