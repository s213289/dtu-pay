package com.dtu.domain.report;

import com.dtu.base.domain.model.event.report.CustomerReportGeneratedEvent;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class CustomerReportResponse {
  private Set<CustomerReportGeneratedEvent.CustomerReport> reports;

  public CustomerReportResponse(CustomerReportGeneratedEvent event) {
    this.reports = event.getReports();
  }
}
