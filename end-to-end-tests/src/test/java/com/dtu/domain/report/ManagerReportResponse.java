package com.dtu.domain.report;

import com.dtu.base.domain.model.event.report.ManagerReportGeneratedEvent;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class ManagerReportResponse {
  private Set<ManagerReportGeneratedEvent.Report> reports;

  public ManagerReportResponse(ManagerReportGeneratedEvent event) {
    this.reports = event.getReports();
  }
}
