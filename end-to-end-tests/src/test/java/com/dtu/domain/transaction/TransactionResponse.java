package com.dtu.domain.transaction;

import com.dtu.base.domain.model.event.transaction.TransactionFinishedEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRejectedEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class TransactionResponse {
  private boolean status;
  private String message;

  public TransactionResponse(TransactionFinishedEvent event) {
    this.message =
        String.format("Payment success. Merchant ID: %s. Amount: %s.", event.getMerchantId(),
            event.getAmount());
    this.status = true;
  }

  public TransactionResponse(TransactionRejectedEvent event) {
    this.message = String.format("Payment failed. Error message: %s", event.getMessage());
    this.status = false;
  }
}
