package com.dtu.transaction;

import com.dtu.domain.Account;
import com.dtu.domain.AccountInfo;
import com.dtu.domain.BankService;
import com.dtu.domain.BankServiceException_Exception;
import com.dtu.domain.BankServiceService;
import com.dtu.domain.User;
import com.dtu.domain.token.TokenRequestRequest;
import com.dtu.domain.token.TokenRequestResponse;
import com.dtu.domain.transaction.TransactionRequest;
import com.dtu.domain.transaction.TransactionResponse;
import com.dtu.domain.user.UserRegisterRequest;
import com.dtu.domain.user.UserRegisterResponse;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import javax.print.attribute.standard.Media;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.mock;

public class TransactionSteps {

    BankService bank = new BankServiceService().getBankServicePort();
    WebTarget baseUrl;

    private User customer = new User();
    private User merchant = new User();
    private String customerBankAccountId, customerId;
    private String merchantBankAccountId, merchantId;
    private List<String> tokens;
    private TransactionResponse transactionResponse;

    /**
     * @author Aleksander Jarmołkowicz - s213289
     */
    @Before
    public void initialize(){
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
    }

    /**
     * @author Aleksander Jarmołkowicz - s213289
     */
    @Given("a customer {string} {string} with CPR number {string} and account balance {int} dkk registered in bank")
    public void registerCustomer(String firstName, String lastName, String cpr, int balance) throws BankServiceException_Exception {
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setCprNumber(cpr);
        customerBankAccountId = bank.createAccountWithBalance(customer, BigDecimal.valueOf(balance));
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Given("the merchant {string} {string} with CPR number {string} and account balance {int} dkk registered in bank")
    public void registerMerchant(String firstName, String lastName, String cpr, int balance) throws BankServiceException_Exception {
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchant.setCprNumber(cpr);
        merchantBankAccountId = bank.createAccountWithBalance(merchant, BigDecimal.valueOf(balance));
    }

    /**
     * @author Aleksander Jarmołkowicz - s213289
     */
    @Given("they are registered in DTU pay")
    public void registerAtDTU(){
        // register customer
        UserRegisterRequest customerRequest = new UserRegisterRequest(customer.getFirstName(), customer.getLastName(), customerBankAccountId);
        customerId = baseUrl.path("customer")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(customerRequest, MediaType.APPLICATION_JSON), UserRegisterResponse.class).getUserId();
        //register merchant
        UserRegisterRequest merchantRequest = new UserRegisterRequest(merchant.getFirstName(), merchant.getLastName(), merchantBankAccountId);
        merchantId = baseUrl.path("merchant")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(merchantRequest, MediaType.APPLICATION_JSON), UserRegisterResponse.class).getUserId();

        assertNotNull(customerId);
        assertNotNull(merchantId);
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Given("customer is registered in DTU pay")
    public void registerCustomerAtDTU(){
        // register customer
        UserRegisterRequest customerRequest = new UserRegisterRequest(customer.getFirstName(), customer.getLastName(), customerBankAccountId);
        customerId = baseUrl.path("customer")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(customerRequest, MediaType.APPLICATION_JSON), UserRegisterResponse.class).getUserId();
        assertNotNull(customerId);
    }

    /**
     * @author Aleksander Jarmołkowicz - s213289
     */
    @Given("the customer requested {int} tokens successfully")
    public void requestTokensForCustomer(int tokenCount){
        // request tokens
        TokenRequestRequest request = new TokenRequestRequest(customerId, tokenCount);

        tokens = baseUrl.path("customer")
            .path("token")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), TokenRequestResponse.class).getTokens();

        assertEquals(tokenCount, tokens.size());
    }

    /**
     * @author Aleksander Jarmołkowicz - s213289
     */
    @When("the merchant request a payment of {int} dkk")
    public void requestPayment(int amount){
        // request payment with merchant id, token, amount
        TransactionRequest request = new TransactionRequest(tokens.get(0), merchantId, BigDecimal.valueOf(amount));
        transactionResponse = baseUrl.path("merchant").path("transaction")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), TransactionResponse.class);
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @When("the merchant request a payment of {int} dkk with invalid token")
    public void theMerchantRequestAPaymentOfDkkWithInvalidToken(int amount) {
        // request payment with merchant id, token, amount
        TransactionRequest request = new TransactionRequest(UUID.randomUUID().toString(), merchantId, BigDecimal.valueOf(amount));
        transactionResponse = baseUrl.path("merchant").path("transaction")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), TransactionResponse.class);
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @When("the merchant request a payment of {int} dkk with invalid merchantId")
    public void theMerchantRequestAPaymentOfDkkWithInvalidMerchantId(int amount) {
        // request payment with merchant id, token, amount
        TransactionRequest request = new TransactionRequest(tokens.get(0), UUID.randomUUID().toString(), BigDecimal.valueOf(amount));
        transactionResponse = baseUrl.path("merchant").path("transaction")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), TransactionResponse.class);
    }

    /**
     * @author Aleksander Jarmołkowicz - s213289
     */
    @Then("the payment executes successfully")
    public void checkPaymentStatus(){
        assertTrue(transactionResponse.isStatus());
    }

    /**
     * @author Aleksander Jarmołkowicz - s213289
     */
    @Then("the merchant balance is {int} dkk")
    public void checkMerchantBalance(int balance) throws BankServiceException_Exception {
        Account account = bank.getAccount(merchantBankAccountId);
        assertEquals(BigDecimal.valueOf(balance), account.getBalance());
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Then("the customer balance is {int} dkk")
    public void checkCustomerBalance(int balance) throws BankServiceException_Exception {
        Account account = bank.getAccount(customerBankAccountId);
        assertEquals(BigDecimal.valueOf(balance), account.getBalance());
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @Then("the payment is not accepted")
    public void paymentNotExecuted(){
        assertFalse(transactionResponse.isStatus());
    }

    /**
     * @author Marta Aleksandra Niewiadomska - s213040
     */
    @After
    public void retireAccounts() throws BankServiceException_Exception {
        if(customerBankAccountId != null){
            bank.retireAccount(customerBankAccountId);
        }
        if(merchantBankAccountId != null){
            bank.retireAccount(merchantBankAccountId);
        }
    }
}
