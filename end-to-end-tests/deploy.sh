#!/bin/bash
set -e
docker image prune -f
docker-compose up -d rabbitmq
sleep 10
docker-compose up -d dtu-pay-gw report-service token-service transaction-service user-service

# @author Aleksander Jarmołkowicz - s213289, based on Hubert Baumeister project