Feature: User registration

    # @author Marta Aleksandra Niewiadomska - s213040
  Scenario: Successfully registered customer
    Given a customer "Adam" "Mickiewicz" with CPR "122333-0987" has a bank account with balance 2000 dkk
    When the customer registers at DTU-Pay
    Then answer contains the id value for a customer

    # @author Marta Ewa Lech - s212481
  Scenario: Successfully registered merchant
    Given a merchant "Maria" "Sklodowska" with CPR "123456-1234" has a bank account with balance 1000 dkk
    When the merchant registers at DTU-Pay
    Then answer contains the id value for a merchant