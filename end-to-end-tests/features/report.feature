Feature: Report features

    # @author Bartłomiej Józef Otto - s203042
  Scenario: Generate report for customer
    Given a customer "Krystyna" "Janda" with CPR number "231266-3456" and account balance 1230 dkk registered in bank and dtu pay
    And a merchant "Andrzej" "Seweryn" with CPR "220455-3456" has a bank account with balance 2500 dkk and registered in bank and dtu pay
    And a customer requests 4 tokens successfully
    And a merchant request a payment of 100 dkk successfully
    And a merchant request a payment of 70 dkk successfully
    When a customer generates a report
    Then customer report consists of 2 items

    # @author Bartłomiej Józef Otto - s203042
  Scenario: Generate report for merchant
    Given a customer "Jozef" "Pilsudski" with CPR number "120420-6789" and account balance 2200 dkk registered in bank and dtu pay
    And a merchant "Taduesz" "Kosciuszko" with CPR "310822-3455" has a bank account with balance 8300 dkk and registered in bank and dtu pay
    And a customer requests 4 tokens successfully
    And a merchant request a payment of 100 dkk successfully
    When a merchant generates a report
    Then  merchant report consists of 1 items

    # @author Bartłomiej Józef Otto - s203042
  Scenario: Generate report for both merchant and customer
    Given a customer "Zbigniew" "Boniek" with CPR number "180933-4433" and account balance 334400 dkk registered in bank and dtu pay
    And a merchant "Kazimierz" "Deyna" with CPR "280622-5783" has a bank account with balance 45600 dkk and registered in bank and dtu pay
    And a customer requests 4 tokens successfully
    And a merchant request a payment of 100 dkk successfully
    And a merchant request a payment of 70 dkk successfully
    And a merchant request a payment of 20 dkk successfully
    When a merchant generates a report
    And a customer generates a report
    Then merchant report consists of 3 items
    And customer report consists of 3 items