Feature: Token features

    # @author Grzegorz Jacek Kot - s200358
  Scenario: Successful 5 tokens generation
    Given a customer "Adam" "Mickiewicz" with CPR "122333-0987" with bank account balance 2000 dkk
    And the customer is registered in DTU Pay
    When the customer requests valid amount of 5 tokens
    Then the customer has 5 unused tokens

    # @author Grzegorz Jacek Kot - s200358
  Scenario: Successful 1 + 5 tokens generation
    Given a customer "Adam" "Mickiewicz" with CPR "122333-0987" with bank account balance 2000 dkk
    And the customer is registered in DTU Pay
    When the customer requests valid amount of 1 tokens
    Then the customer has 1 unused tokens
    When the customer requests valid amount of 5 tokens
    Then the customer has 6 unused tokens

    # @author Marta Ewa Lech - s212481
  Scenario: Unsuccessful 6 tokens generation
    Given a customer "Adam" "Mickiewicz" with CPR "122333-0987" with bank account balance 2000 dkk
    And the customer is registered in DTU Pay
    When the customer requests valid amount of 6 tokens
    Then the customer has 0 unused tokens

    # @author Marta Ewa Lech - s212481
  Scenario: Unsuccessful 2 + 3 tokens generation
    Given a customer "Adam" "Mickiewicz" with CPR "122333-0987" with bank account balance 2000 dkk
    And the customer is registered in DTU Pay
    When the customer requests valid amount of 2 tokens
    Then the customer has 2 unused tokens
    When the customer requests valid amount of 3 tokens
    Then the customer has 2 unused tokens

    # @author Aleksander Jarmołkowicz - s213289
  Scenario: Unsuccessful 0 tokens generation
    Given a customer "Adam" "Mickiewicz" with CPR "122333-0987" with bank account balance 2000 dkk
    And the customer is registered in DTU Pay
    When the customer requests invalid amount of 0 tokens
    Then the customer received an error meassge

    # @author Marta Aleksandra Niewiadomska - s213040
  Scenario: Unsuccessful -1 tokens generation
    Given a customer "Adam" "Mickiewicz" with CPR "122333-0987" with bank account balance 2000 dkk
    And the customer is registered in DTU Pay
    When the customer requests invalid amount of -1 tokens
    Then the customer received an error meassge
