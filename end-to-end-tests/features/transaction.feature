Feature: Transaction

    # @author Aleksander Jarmołkowicz - s213289
  Scenario: Successful transaction
      Given a customer "Robert" "Maklowicz" with CPR number "345678-9999" and account balance 20000 dkk registered in bank
    And the merchant "Magda" "Gessler" with CPR number "345678-9998" and account balance 2000 dkk registered in bank
    And they are registered in DTU pay
    And the customer requested 4 tokens successfully
    When the merchant request a payment of 100 dkk
    Then the payment executes successfully
    And the merchant balance is 2100 dkk
    And the customer balance is 19900 dkk

    # @author Aleksander Jarmołkowicz - s213289
  Scenario: Not enough money
    Given a customer "Fryderyk" "Chopin" with CPR number "345678-9999" and account balance 200 dkk registered in bank
    And the merchant "Stanislaw" "Moniuszko" with CPR number "345678-9998" and account balance 2000 dkk registered in bank
    And they are registered in DTU pay
    And the customer requested 3 tokens successfully
    When the merchant request a payment of 250 dkk
    Then the payment is not accepted

    # @author Marta Aleksandra Niewiadomska - s213040
  Scenario: Invalid token
    Given a customer "Boleslaw" "Chrobry" with CPR number "345678-9999" and account balance 500 dkk registered in bank
    And the merchant "Stefan" "Batory" with CPR number "345678-9998" and account balance 6000 dkk registered in bank
    And they are registered in DTU pay
    When the merchant request a payment of 230 dkk with invalid token
    Then the payment is not accepted

    # @author Marta Aleksandra Niewiadomska - s213040
  Scenario: Merchant not registered at DTU pay
    Given a customer "Czeslaw" "Milosz" with CPR number "345678-9999" and account balance 1200 dkk registered in bank
    And the merchant "Wislawa" "Szymborska" with CPR number "345678-9998" and account balance 90000 dkk registered in bank
    And customer is registered in DTU pay
    And the customer requested 4 tokens successfully
    When the merchant request a payment of 30 dkk with invalid merchantId
    Then the payment is not accepted