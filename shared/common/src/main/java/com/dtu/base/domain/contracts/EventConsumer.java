package com.dtu.base.domain.contracts;

import com.dtu.base.domain.model.BusinessEvent;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@FunctionalInterface
public interface EventConsumer<T extends BusinessEvent> {
  void accept(T event);
}
