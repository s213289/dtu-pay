/**
 * I am not a great fan of "utils" packages. Let's monitor how does the growth speed looks like in time
 * If there are too many classes we should delegate the relevant functionalities accordingly
 */

package com.dtu.base.util;
