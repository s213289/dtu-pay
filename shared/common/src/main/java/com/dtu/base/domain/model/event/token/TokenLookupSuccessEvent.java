package com.dtu.base.domain.model.event.token;

import com.dtu.base.domain.model.BusinessEvent;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
@Getter @Setter @ToString
public class TokenLookupSuccessEvent extends BusinessEvent {
  private UUID customerId;
  private UUID tokenId;
}
