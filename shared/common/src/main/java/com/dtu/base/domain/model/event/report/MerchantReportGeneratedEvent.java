package com.dtu.base.domain.model.event.report;

import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.base.domain.model.event.transaction.TransactionFinishedEvent;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class MerchantReportGeneratedEvent extends BusinessEvent {
  private Set<MerchantReport> reports;

  @AllArgsConstructor @NoArgsConstructor @Getter @Setter
  public static class MerchantReport {
    private String token;
    private BigDecimal amount;
    private LocalDateTime timestamp;
  }
}
