package com.dtu.base.domain.contracts;

import com.dtu.base.domain.model.BusinessEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public interface EventDispatcher {
  Map<Class<? extends BusinessEvent>, List<EventConsumer<? extends BusinessEvent>>> consumers = new ConcurrentHashMap<>();

  default void register(Class<? extends BusinessEvent> clazz, EventConsumer<? extends BusinessEvent> consumer) {
    if (!consumers.containsKey(clazz)) {
      registerSpecific(clazz);
      consumers.put(clazz, new LinkedList<>());
    }
    consumers.get(clazz).add(consumer);
  }

  void registerSpecific(Class<? extends BusinessEvent> clazz);

  default void dispatch(Class<? extends BusinessEvent> clazz, BusinessEvent event) {
    for (EventConsumer consumer : consumers.get(clazz)) {
      consumer.accept(event);
    }
  }
}
