package com.dtu.base.domain.model.event.user.merchant;

import com.dtu.base.domain.model.BusinessEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Marta Ewa Lech - s212481
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MerchantRegistrationResponseEvent extends BusinessEvent {
    String merchantId;

    public MerchantRegistrationResponseEvent(String merchantId){
        super();
        this.merchantId = merchantId;
    }
}
