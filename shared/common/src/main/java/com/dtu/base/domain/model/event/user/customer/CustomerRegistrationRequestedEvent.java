package com.dtu.base.domain.model.event.user.customer;

import com.dtu.base.domain.model.BusinessEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
@NoArgsConstructor
@Getter @Setter @ToString
public class CustomerRegistrationRequestedEvent extends BusinessEvent {
    String firstName;
    String lastName;
    String bankAccountId;
    String userId;

    public CustomerRegistrationRequestedEvent(String firstName, String lastName, String bankAccountId) {
        super();
        this.firstName = firstName;
        this.lastName =  lastName;
        this.bankAccountId = bankAccountId;
    }
}
