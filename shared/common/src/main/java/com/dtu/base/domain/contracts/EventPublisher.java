package com.dtu.base.domain.contracts;

import com.dtu.base.domain.model.BusinessEvent;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public interface EventPublisher {
  <T extends BusinessEvent> void publish(T event);
}
