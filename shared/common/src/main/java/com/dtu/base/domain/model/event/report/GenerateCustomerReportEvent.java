package com.dtu.base.domain.model.event.report;

import com.dtu.base.domain.model.BusinessEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class GenerateCustomerReportEvent extends BusinessEvent {
  private String customerId;
}
