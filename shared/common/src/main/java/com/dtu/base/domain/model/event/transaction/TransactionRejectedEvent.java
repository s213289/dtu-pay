package com.dtu.base.domain.model.event.transaction;

import com.dtu.base.domain.model.BusinessEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class TransactionRejectedEvent extends BusinessEvent {
  private String message;
}
