package com.dtu.base.domain.model.event.report;

import com.dtu.base.domain.model.BusinessEvent;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class ManagerReportGeneratedEvent extends BusinessEvent {
  private Set<Report> reports;

  @AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
  public static class Report {
    private String token;
    private String customerId;
    private String merchantId;
    private String customerBankAccount;
    private String merchantBankAccount;
    private BigDecimal amount;
    private LocalDateTime timestamp;
  }
}
