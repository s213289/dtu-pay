package com.dtu.base.domain.model.event.report;

import com.dtu.base.domain.model.BusinessEvent;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@Getter @Setter @ToString
public class GenerateManagerReportEvent extends BusinessEvent {
}
