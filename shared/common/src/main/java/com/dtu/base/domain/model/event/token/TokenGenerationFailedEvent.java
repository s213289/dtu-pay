package com.dtu.base.domain.model.event.token;

import com.dtu.base.domain.model.BusinessEvent;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
@Getter @Setter @ToString
public class TokenGenerationFailedEvent extends BusinessEvent {
    private String message;
}
