package com.dtu.base.domain.model;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@Getter @Setter @ToString
public class BusinessEvent {
  protected String id;
  protected String flowId;
  protected LocalDateTime timestamp;

  public BusinessEvent() {
    this.id = UUID.randomUUID().toString();
    this.timestamp = LocalDateTime.now();
  }
}
