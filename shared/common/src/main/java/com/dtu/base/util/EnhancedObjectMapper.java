package com.dtu.base.util;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public class EnhancedObjectMapper extends ObjectMapper {
  public EnhancedObjectMapper() {
    this.findAndRegisterModules();
  }
}
