package com.dtu.base.domain.model.event.user.customer;

import com.dtu.base.domain.model.BusinessEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
@NoArgsConstructor
@Getter @Setter @ToString
public class CustomerBankAccountIdRetrievedEvent extends BusinessEvent {
  private String bankAccountId;

  public CustomerBankAccountIdRetrievedEvent(String bankAccountId){
    super();
    this.bankAccountId = bankAccountId;
  }
}
