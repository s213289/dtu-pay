package com.dtu.base.domain.model.event.user.merchant;

import com.dtu.base.domain.model.BusinessEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Marta Ewa Lech - s212481
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MerchantRegistrationRequestedEvent extends BusinessEvent {
    private String firstName;
    private String lastName;
    private String bankAccountId;
    private String userID;

    public MerchantRegistrationRequestedEvent(String firstName, String lastName, String bankAccountId){
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.bankAccountId = bankAccountId;
    }
}
