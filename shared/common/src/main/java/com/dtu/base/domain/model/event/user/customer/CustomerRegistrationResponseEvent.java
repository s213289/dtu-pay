package com.dtu.base.domain.model.event.user.customer;

import com.dtu.base.domain.model.BusinessEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CustomerRegistrationResponseEvent extends BusinessEvent {
    String userId;

    public CustomerRegistrationResponseEvent(String userId){
        super();
        this.userId = userId;
    }
}
