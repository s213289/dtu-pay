package com.dtu.base.domain.model.event.transaction;

import com.dtu.base.domain.model.BusinessEvent;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class TransactionFinishedEvent extends BusinessEvent {
  private String token;
  private String customerId;
  private String merchantId;
  private String customerAccount;
  private String merchantAccount;
  private BigDecimal amount;
}
