package com.dtu.base.domain.model.event.transaction;

import com.dtu.base.domain.model.BusinessEvent;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@NoArgsConstructor @Getter @Setter @ToString
public class TransactionRequestedEvent extends BusinessEvent {
  private String token;
  private String merchantId;
  private BigDecimal amount;

  public TransactionRequestedEvent(String token, String merchantId, BigDecimal amount) {
    this.token = token;
    this.merchantId = merchantId;
    this.amount = amount;
    this.flowId = UUID.randomUUID().toString();
  }
}
