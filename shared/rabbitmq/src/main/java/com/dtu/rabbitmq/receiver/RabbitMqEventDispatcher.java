package com.dtu.rabbitmq.receiver;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.rabbitmq.configuration.RabbitMqBased;
import com.dtu.rabbitmq.configuration.RabbitMqTenantConfig;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.rabbitmq.client.DeliverCallback;
import java.io.IOException;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@Slf4j
public class RabbitMqEventDispatcher extends RabbitMqBased implements EventDispatcher {
  @Override public void registerSpecific(Class<? extends BusinessEvent> clazz) {
    try {
      String queue = channel.queueDeclare(UUID.randomUUID().toString(), false, false, true, null).getQueue();
      channel.queueBind(queue, RabbitMqTenantConfig.EXCHANGE, clazz.getName());
      DeliverCallback deliverCallback = (tag, msg) -> {
        try {
          dispatch(clazz, mapper.readValue(msg.getBody(), clazz));
        } catch (JsonParseException | JsonMappingException je) {
          log.error("Could not parse event message from Rabbit MQ queue. Error: {}", je.getMessage());
        }
      };
      channel.basicConsume(queue, true, deliverCallback, consumerTag -> {});
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
