package com.dtu.rabbitmq.configuration;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public class RabbitMqTenantConfig {
  public static final String HOST = "rabbitmq";
  public static final String EXCHANGE = "dtu-pay-events";
}
