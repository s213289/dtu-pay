package com.dtu.rabbitmq.sender;

import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.BusinessEvent;
import com.dtu.rabbitmq.configuration.RabbitMqBased;
import com.dtu.rabbitmq.configuration.RabbitMqTenantConfig;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@Slf4j
public class RabbitMqEventPublisher extends RabbitMqBased implements EventPublisher {
  @Override public <T extends BusinessEvent> void publish(T event) {
      try {
        channel.basicPublish(RabbitMqTenantConfig.EXCHANGE, event.getClass().getName(), null, mapper.writeValueAsBytes(event));
      } catch (JsonProcessingException e) {
        log.error("Could not send {} event using Rabbit MQ. Event payload could not be stringified. Error message: {}", event, e.getMessage());
      } catch (IOException e) {
        log.error("Could not send {} event using Rabbit MQ. Message could not be published. Error message: {}", event, e.getMessage());
      }
  }
}
