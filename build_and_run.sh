#!/bin/bash
set -e

./build.sh

pushd end-to-end-tests
./deploy.sh

sleep 5

./test.sh
popd

docker image prune -f

# @author Aleksander Jarmołkowicz - s213289, based on Hubert Baumeister project
