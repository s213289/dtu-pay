#!/bin/bash
set -e

mvn clean install package

pushd dtu-pay-gw
docker build -t dtu-pay-gw .
popd

pushd report
docker build -t report-service .
popd

pushd token
docker build -t token-service .
popd

pushd transaction
docker build -t transaction-service .
popd

pushd user
docker build -t user-service .
popd

# @author Aleksander Jarmołkowicz - s213289, based on Hubert Baumeister project