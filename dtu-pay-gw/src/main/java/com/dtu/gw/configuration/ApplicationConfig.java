package com.dtu.gw.configuration;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.gw.application.PaymentService;
import com.dtu.gw.application.ReportService;
import com.dtu.gw.application.TokenService;
import com.dtu.gw.application.UserService;
import com.dtu.rabbitmq.receiver.RabbitMqEventDispatcher;
import com.dtu.rabbitmq.sender.RabbitMqEventPublisher;
import io.quarkus.runtime.Startup;
import javax.enterprise.context.ApplicationScoped;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
public class ApplicationConfig {
  @Startup @ApplicationScoped
  public UserService userService(EventPublisher publisher, EventDispatcher dispatcher) {
    return new UserService(publisher, dispatcher);
  }

  @Startup @ApplicationScoped
  public PaymentService paymentService(EventPublisher publisher, EventDispatcher dispatcher) {
    return new PaymentService(publisher, dispatcher);
  }

  @Startup @ApplicationScoped
  public TokenService tokenService(EventPublisher publisher, EventDispatcher dispatcher) {
    return new TokenService(publisher, dispatcher);
  }

  @Startup @ApplicationScoped
  public ReportService reportService(EventPublisher publisher, EventDispatcher dispatcher) {
    return new ReportService(publisher, dispatcher);
  }

  @ApplicationScoped
  public EventDispatcher eventDispatcher() {
    return new RabbitMqEventDispatcher();
  }

  @ApplicationScoped
  public EventPublisher eventPublisher() {
    return new RabbitMqEventPublisher();
  }
}
