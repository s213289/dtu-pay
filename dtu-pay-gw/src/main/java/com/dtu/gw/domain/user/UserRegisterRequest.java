package com.dtu.gw.domain.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Marta Ewa Lech - s212481
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class UserRegisterRequest {
  private String firstName;
  private String lastName;
  private String bankAccountId;
}
