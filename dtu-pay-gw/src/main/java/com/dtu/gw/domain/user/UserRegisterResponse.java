package com.dtu.gw.domain.user;

import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationResponseEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationResponseEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Marta Aleksandra Niewiadomska - s213040
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class UserRegisterResponse {
  private String userId;

  public UserRegisterResponse(CustomerRegistrationResponseEvent event) {
    this.userId = event.getUserId();
  }

  public UserRegisterResponse(MerchantRegistrationResponseEvent event) {
    this.userId = event.getMerchantId();
  }
}
