package com.dtu.gw.domain.report;

import com.dtu.base.domain.model.event.report.ManagerReportGeneratedEvent;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class ManagerReportResponse {
  private Set<ManagerReportGeneratedEvent.Report> reports;

  public ManagerReportResponse(ManagerReportGeneratedEvent event) {
    this.reports = event.getReports();
  }
}
