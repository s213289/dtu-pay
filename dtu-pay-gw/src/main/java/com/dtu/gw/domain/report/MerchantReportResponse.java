package com.dtu.gw.domain.report;

import com.dtu.base.domain.model.event.report.MerchantReportGeneratedEvent;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Bartłomiej Józef Otto - s203042
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class MerchantReportResponse {
  private Set<MerchantReportGeneratedEvent.MerchantReport> reports;

  public MerchantReportResponse(MerchantReportGeneratedEvent event) {
    this.reports = event.getReports();
  }
}
