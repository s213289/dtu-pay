package com.dtu.gw.domain.token;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class TokenRequestRequest {
  private String customerId;
  private Integer amount;
}
