package com.dtu.gw.domain.token;

import com.dtu.base.domain.model.event.token.TokenGenerationFailedEvent;
import com.dtu.base.domain.model.event.token.TokenGenerationSuccessEvent;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class TokenRequestResponse {
  private String message;
  private List<String> tokens;

  public TokenRequestResponse(TokenGenerationSuccessEvent event) {
    this.message =
        String.format("Token generation successful for %s.", event.getCustomerId());
    this.tokens = event.getTokens().stream().map(UUID::toString).collect(Collectors.toList());
  }

  public TokenRequestResponse(TokenGenerationFailedEvent event) {
    this.message = String.format("Token generation failed. %s", event.getMessage());
  }
}
