package com.dtu.gw.domain.transaction;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class TransactionRequest {
  private String token;
  private String merchantId;
  private BigDecimal amount;
}
