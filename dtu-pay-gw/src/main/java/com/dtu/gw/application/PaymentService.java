package com.dtu.gw.application;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.transaction.TransactionFinishedEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRejectedEvent;
import com.dtu.base.domain.model.event.transaction.TransactionRequestedEvent;
import com.dtu.gw.domain.transaction.TransactionRequest;
import com.dtu.gw.domain.transaction.TransactionResponse;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
public class PaymentService {
  private final EventPublisher publisher;
  private final EventDispatcher dispatcher;

  private final Map<String, CompletableFuture<TransactionResponse>> correlations;

  public PaymentService(EventPublisher publisher, EventDispatcher dispatcher) {
    this.correlations = new ConcurrentHashMap<>();
    this.publisher = publisher;
    this.dispatcher = dispatcher;

    this.dispatcher.register(TransactionFinishedEvent.class,
        $ -> handleSuccess((TransactionFinishedEvent) $));
    this.dispatcher.register(TransactionRejectedEvent.class,
        $ -> handleFailure((TransactionRejectedEvent) $));
  }

  public TransactionResponse pay(TransactionRequest request) {
    String flowId = UUID.randomUUID().toString();
    correlations.put(flowId, new CompletableFuture<>());

    TransactionRequestedEvent event =
        new TransactionRequestedEvent(request.getToken(),
            request.getMerchantId(),
            request.getAmount()
        );

    event.setFlowId(flowId);

    publisher.publish(event);
    return correlations.get(flowId).join();
  }

  private void handleSuccess(TransactionFinishedEvent event) {
    TransactionResponse response = new TransactionResponse(event);
    CompletableFuture<TransactionResponse> future = correlations.get(event.getFlowId());
    future.complete(response);
  }

  private void handleFailure(TransactionRejectedEvent event) {
    TransactionResponse response = new TransactionResponse(event);
    CompletableFuture<TransactionResponse> future = correlations.get(event.getFlowId());
    future.complete(response);
  }
}
