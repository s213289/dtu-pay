package com.dtu.gw.application;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.report.CustomerReportGeneratedEvent;
import com.dtu.base.domain.model.event.report.GenerateCustomerReportEvent;
import com.dtu.base.domain.model.event.report.GenerateManagerReportEvent;
import com.dtu.base.domain.model.event.report.GenerateMerchantReportEvent;
import com.dtu.base.domain.model.event.report.ManagerReportGeneratedEvent;
import com.dtu.base.domain.model.event.report.MerchantReportGeneratedEvent;
import com.dtu.gw.domain.report.CustomerReportResponse;
import com.dtu.gw.domain.report.ManagerReportResponse;
import com.dtu.gw.domain.report.MerchantReportResponse;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class ReportService {
  private final EventPublisher publisher;
  private final EventDispatcher dispatcher;

  private final Map<String, CompletableFuture<MerchantReportResponse>> merchantCorrelations;
  private final Map<String, CompletableFuture<CustomerReportResponse>> customerCorrelations;
  private final Map<String, CompletableFuture<ManagerReportResponse>> managerCorrelations;

  public ReportService(EventPublisher publisher, EventDispatcher dispatcher) {
    this.merchantCorrelations = new ConcurrentHashMap<>();
    this.customerCorrelations = new ConcurrentHashMap<>();
    this.managerCorrelations = new ConcurrentHashMap<>();

    this.publisher = publisher;
    this.dispatcher = dispatcher;

    this.dispatcher.register(ManagerReportGeneratedEvent.class,
        $ -> handleManager((ManagerReportGeneratedEvent) $));
    this.dispatcher.register(CustomerReportGeneratedEvent.class,
        $ -> handleCustomer((CustomerReportGeneratedEvent) $));
    this.dispatcher.register(MerchantReportGeneratedEvent.class,
        $ -> handleMerchant((MerchantReportGeneratedEvent) $));
  }

  /**
   * @author Aleksander Jarmołkowicz - s213289
   */
  public ManagerReportResponse generateForManager() {
    String flowId = UUID.randomUUID().toString();
    managerCorrelations.put(flowId, new CompletableFuture<>());

    GenerateManagerReportEvent event = new GenerateManagerReportEvent();

    event.setFlowId(flowId);

    publisher.publish(event);
    return managerCorrelations.get(flowId).join();
  }

  /**
   * @author Bartłomiej Józef Otto - s203042
   */
  public CustomerReportResponse generateForCustomer(String customerId) {
    String flowId = UUID.randomUUID().toString();
    customerCorrelations.put(flowId, new CompletableFuture<>());

    GenerateCustomerReportEvent event = new GenerateCustomerReportEvent(customerId);

    event.setFlowId(flowId);

    publisher.publish(event);
    return customerCorrelations.get(flowId).join();
  }

  /**
   * @author Bartłomiej Józef Otto - s203042
   */
  public MerchantReportResponse generateForMerchant(String merchantId) {
    String flowId = UUID.randomUUID().toString();
    merchantCorrelations.put(flowId, new CompletableFuture<>());

    GenerateMerchantReportEvent event = new GenerateMerchantReportEvent(merchantId);

    event.setFlowId(flowId);

    publisher.publish(event);
    return merchantCorrelations.get(flowId).join();
  }

  /**
   * @author Aleksander Jarmołkowicz - s213289
   */
  private void handleManager(ManagerReportGeneratedEvent event) {
    ManagerReportResponse response = new ManagerReportResponse(event);
    CompletableFuture<ManagerReportResponse> future = managerCorrelations.get(event.getFlowId());
    future.complete(response);
  }

  /**
   * @author Bartłomiej Józef Otto - s203042
   */
  private void handleCustomer(CustomerReportGeneratedEvent event) {
    CustomerReportResponse response = new CustomerReportResponse(event);
    CompletableFuture<CustomerReportResponse> future = customerCorrelations.get(event.getFlowId());
    future.complete(response);
  }

  /**
   * @author Bartłomiej Józef Otto - s203042
   */
  private void handleMerchant(MerchantReportGeneratedEvent event) {
    MerchantReportResponse response = new MerchantReportResponse(event);
    CompletableFuture<MerchantReportResponse> future = merchantCorrelations.get(event.getFlowId());
    future.complete(response);
  }
}
