package com.dtu.gw.application.adapter.rest;

import com.dtu.gw.application.PaymentService;
import com.dtu.gw.application.ReportService;
import com.dtu.gw.application.UserService;
import com.dtu.gw.domain.report.MerchantReportResponse;
import com.dtu.gw.domain.transaction.TransactionRequest;
import com.dtu.gw.domain.transaction.TransactionResponse;
import com.dtu.gw.domain.user.UserRegisterRequest;
import com.dtu.gw.domain.user.UserRegisterResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@Path("/merchant")
public class MerchantFacade {
  private final PaymentService paymentService;
  private final UserService userService;
  private final ReportService reportService;

  public MerchantFacade(PaymentService paymentService, UserService userService, ReportService reportService) {
    this.paymentService = paymentService;
    this.userService = userService;
    this.reportService = reportService;
  }

  /**
   * @author Marta Aleksandra Niewiadomska - s213040
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public UserRegisterResponse register(UserRegisterRequest request) {
    return userService.registerMerchant(request);
  }

  /**
   * @author Aleksander Jarmołkowicz - s213289
   */
  @POST
  @Path("/transaction")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public TransactionResponse requestTransaction(TransactionRequest request) {
    return paymentService.pay(request);
  }

  /**
   * @author Bartłomiej Józef Otto - s203042
   */
  @GET
  @Path("/report/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public MerchantReportResponse generateReport(@PathParam("id") String id) {
    return reportService.generateForMerchant(id);
  }
}
