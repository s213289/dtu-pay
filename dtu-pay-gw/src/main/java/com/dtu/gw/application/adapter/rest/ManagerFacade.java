package com.dtu.gw.application.adapter.rest;

import com.dtu.gw.application.ReportService;
import com.dtu.gw.domain.report.ManagerReportResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Aleksander Jarmołkowicz - s213289
 */
@Path("/manager")
public class ManagerFacade {
  private final ReportService reportService;

  public ManagerFacade(ReportService reportService) {
    this.reportService = reportService;
  }

  @GET
  @Path("/report")
  @Produces(MediaType.APPLICATION_JSON)
  public ManagerReportResponse requestTransaction() {
    return reportService.generateForManager();
  }
}
