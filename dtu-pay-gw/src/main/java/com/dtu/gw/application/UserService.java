package com.dtu.gw.application;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationRequestedEvent;
import com.dtu.base.domain.model.event.user.customer.CustomerRegistrationResponseEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationRequestedEvent;
import com.dtu.base.domain.model.event.user.merchant.MerchantRegistrationResponseEvent;
import com.dtu.gw.domain.user.UserRegisterRequest;
import com.dtu.gw.domain.user.UserRegisterResponse;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;


public class UserService {
  private final EventPublisher publisher;
  private final EventDispatcher dispatcher;

  private final Map<String, CompletableFuture<UserRegisterResponse>> merchantCorrelations;
  private final Map<String, CompletableFuture<UserRegisterResponse>> customerCorrelations;

  public UserService(EventPublisher publisher, EventDispatcher dispatcher) {
    this.merchantCorrelations = new ConcurrentHashMap<>();
    this.customerCorrelations = new ConcurrentHashMap<>();
    this.publisher = publisher;
    this.dispatcher = dispatcher;

    this.dispatcher.register(MerchantRegistrationResponseEvent.class,
        $ -> handle((MerchantRegistrationResponseEvent) $));
    this.dispatcher.register(CustomerRegistrationResponseEvent.class,
        $ -> handle((CustomerRegistrationResponseEvent) $));
  }

  /**
   * @author Marta Ewa Lech - s212481
   */
  public UserRegisterResponse registerCustomer(UserRegisterRequest request) {
    String flowId = UUID.randomUUID().toString();
    customerCorrelations.put(flowId, new CompletableFuture<>());

    CustomerRegistrationRequestedEvent event =
        new CustomerRegistrationRequestedEvent(
            request.getFirstName(),
            request.getLastName(),
            request.getBankAccountId()
        );

    event.setFlowId(flowId);

    publisher.publish(event);
    return customerCorrelations.get(flowId).join();
  }

  /**
   * @author Marta Aleksandra Niewiadomska - s213040
   */
  public UserRegisterResponse registerMerchant(UserRegisterRequest request) {
    String flowId = UUID.randomUUID().toString();
    customerCorrelations.put(flowId, new CompletableFuture<>());

    MerchantRegistrationRequestedEvent event =
        new MerchantRegistrationRequestedEvent(
            request.getFirstName(),
            request.getLastName(),
            request.getBankAccountId()
        );

    event.setFlowId(flowId);

    publisher.publish(event);
    return customerCorrelations.get(flowId).join();
  }

  /**
   * @author Marta Ewa Lech - s212481
   */
  private void handle(CustomerRegistrationResponseEvent event) {
    UserRegisterResponse response = new UserRegisterResponse(event);
    CompletableFuture<UserRegisterResponse> future = customerCorrelations.get(event.getFlowId());
    future.complete(response);
  }

  /**
   * @author Marta Aleksandra Niewiadomska - s213040
   */
  private void handle(MerchantRegistrationResponseEvent event) {
    UserRegisterResponse response = new UserRegisterResponse(event);
    CompletableFuture<UserRegisterResponse> future = customerCorrelations.get(event.getFlowId());
    future.complete(response);
  }
}
