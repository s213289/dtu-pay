package com.dtu.gw.application.adapter.rest;

import com.dtu.gw.application.ReportService;
import com.dtu.gw.application.TokenService;
import com.dtu.gw.application.UserService;
import com.dtu.gw.domain.report.CustomerReportResponse;
import com.dtu.gw.domain.token.TokenRequestRequest;
import com.dtu.gw.domain.token.TokenRequestResponse;
import com.dtu.gw.domain.user.UserRegisterRequest;
import com.dtu.gw.domain.user.UserRegisterResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/customer")
public class CustomerFacade {
  private final UserService userService;
  private final TokenService tokenService;
  private final ReportService reportService;

  public CustomerFacade(UserService userService, TokenService tokenService, ReportService reportService) {
    this.userService = userService;
    this.tokenService = tokenService;
    this.reportService = reportService;
  }

  /**
   * @author Marta Ewa Lech - s212481
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public UserRegisterResponse register(UserRegisterRequest request) {
    return userService.registerCustomer(request);
  }

  /**
   * @author Grzegorz Jacek Kot - s200358
   */
  @POST
  @Path("/token")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public TokenRequestResponse requestToken(TokenRequestRequest request) {
    return tokenService.requestToken(request);
  }

  /**
   * @author Bartłomiej Józef Otto - s203042
   */
  @GET
  @Path("/report/{id}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public CustomerReportResponse generateReport(@PathParam("id") String id) {
    return reportService.generateForCustomer(id);
  }
}
