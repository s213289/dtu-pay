package com.dtu.gw.application;

import com.dtu.base.domain.contracts.EventDispatcher;
import com.dtu.base.domain.contracts.EventPublisher;
import com.dtu.base.domain.model.event.token.TokenGenerateEvent;
import com.dtu.base.domain.model.event.token.TokenGenerationFailedEvent;
import com.dtu.base.domain.model.event.token.TokenGenerationSuccessEvent;
import com.dtu.gw.domain.token.TokenRequestRequest;
import com.dtu.gw.domain.token.TokenRequestResponse;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Grzegorz Jacek Kot - s200358
 */
public class TokenService {
  private final EventPublisher publisher;
  private final Map<String, CompletableFuture<TokenRequestResponse>> correlations;

  public TokenService(EventPublisher publisher,
      EventDispatcher dispatcher) {
    this.correlations = new ConcurrentHashMap<>();
    this.publisher = publisher;

    dispatcher.register(TokenGenerationSuccessEvent.class,
        $ -> handleSuccess((TokenGenerationSuccessEvent) $));
    dispatcher.register(TokenGenerationFailedEvent.class,
        $ -> handleFailure((TokenGenerationFailedEvent) $));
  }

  private void handleSuccess(TokenGenerationSuccessEvent event) {
    TokenRequestResponse response = new TokenRequestResponse(event);
    CompletableFuture<TokenRequestResponse> future = correlations.get(event.getFlowId());
    future.complete(response);
  }

  private void handleFailure(TokenGenerationFailedEvent event) {
    TokenRequestResponse response = new TokenRequestResponse(event);
    CompletableFuture<TokenRequestResponse> future = correlations.get(event.getFlowId());
    future.complete(response);
  }

  public TokenRequestResponse requestToken(TokenRequestRequest request) {
    String flowId = UUID.randomUUID().toString();
    correlations.put(flowId, new CompletableFuture<>());

    TokenGenerateEvent event = new TokenGenerateEvent();
    event.setCustomerId(UUID.fromString(request.getCustomerId()));
    event.setAmount(request.getAmount());
    event.setFlowId(flowId);

    publisher.publish(event);
    return correlations.get(flowId).join();
  }
}
